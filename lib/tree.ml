open Core

(**
Le type ('n, 'l, 'b) t représente un nœud (Node) ou une feuille (Leaf) d'un arbre. 
Il est paramétré par trois types génériques 'n, 'l, et 'b qui correspondent respectivement 
aux types de données pour les noeuds, les feuilles et les branches de l'arbre.  
*)
type ('n, 'l, 'b) t =
  | Node of {
    (*
      Pour un noeud (Node), il contient un champ data de type 'n qui représente 
      les données spécifiques au nœud, et un champ branches de type ('n, 'l, 'b) 
      branch List1.t qui représente une liste de branches attachées au nœud.   
    *)
      data : 'n ;
      branches : ('n, 'l, 'b) branch List1.t ;
    }
  | Leaf of 'l  (*Pour une feuille (Leaf), il contient simplement un champ data de type 'l qui représente les 
     données spécifiques à la feuille.*)
(*
Le type ('n, 'l, 'b) branch représente une branche d'un arbre. 
Il est également paramétré par les mêmes types génériques 'n, 'l, et 'b.   
*)
and ('n, 'l, 'b) branch = Branch of {
    data : 'b ; (*Le champ data de type 'b représente les données spécifiques à la branche.*)
    tip : ('n, 'l, 'b) t ; (*Le champ tip de type ('n, 'l, 'b) t représente le nœud ou la feuille attachée à la branche.
       Il s'agit donc du sous arbre auquel est attaché la branche*)
  }


[@@deriving sexp]



type ('n, 'l, 'b) tree_extended = {
    tree: ('n, 'l, 'b) t;  (* L'arbre de type ('n, 'l, 'b) t *)
    node_count: int;       (* Le nombre de nœuds de l'arbre *)
}





let height t =
  let rec calculate_height t acc =
    match t with
    | Leaf _ -> acc  (* La hauteur d'une feuille est égale à l'accumulateur *)
    | Node n ->
      (* Calculer la hauteur de chaque sous-arbre associé aux branches *)
      let branch_heights = List1.map n.branches ~f:(fun (Branch b) -> calculate_height b.tip acc) in
      (* La hauteur de l'arbre est la hauteur maximale parmi les sous-arbres, plus 1 pour le nœud actuel *)
      1 + List1.fold branch_heights ~init:0 ~f:max
  in
  calculate_height t 0    


(* Parcours l'arbre, en enregistrant à chaque descente l'information du parent (que tu applique à la fonction f) *)
let preorder_traversal_with_parent root ~f =
  let rec aux child_node parent_data branch_info =
    match child_node with
    | Leaf l -> f l parent_data branch_info
    | Node n ->
      (* Appliquer la fonction f au nœud *)
      f n.data parent_data branch_info ;
      (* Appliquer la fonction f à chaque sous-arbre associé aux branches *)
      List1.iter n.branches ~f:(fun (Branch b) -> aux b.tip n.data b.data)
  in
  let aux2 root =
    match root with
    | Leaf _ -> ()
    | Node n ->
      (* Appliquer la fonction f à chaque sous-arbre associé aux branches *)
      List1.iter n.branches ~f:(fun (Branch b) -> aux b.tip n.data b.data)
  in
  aux2 root


let leaf l = Leaf l (* Crée une feuille à partir d'une donnée de type 'l. *)

let node data branches =
  Node { data ; branches } (* Crée un noeud à partir d'une donnée de type 'n et d'une liste de branches. *)

let binary_node data left right =
  Node { data ; branches = List1.cons left [ right ] } (* Crée un noeud binaire à partir d'une donnée de type 'n et de deux branches. *)

let branch data tip = Branch { data ; tip } (* Crée une branche à partir d'une donnée de type 'b et d'un nœud ou d'une feuille. *)

let data = function
  | Node n -> n.data
  | Leaf l -> l (* Retourne les données spécifiques à un nœud ou une feuille. *)

module B = PrintBox (* Alias pour le module PrintBox. Il s'agit d'un modèle générant une structure d'arbre imprimable. *)


(*
   Fonction auxiliaire récursive pour générer la structure d'arbre imprimable (PrintBox.tree) à partir d'un arbre.
   Les paramètres parent_branch, node, leaf et branch sont utilisés comme fonctions de rappel pour déterminer le texte à afficher pour chaque élément.
   La fonction est appelée de manière récursive pour traiter les sous-arbres attachés aux branches.
*)
let rec to_printbox_aux t ?parent_branch ~node ~leaf ~branch () = 
  match t with
  | Leaf l -> 
    (* Si t est une feuille (Leaf l), la fonction leaf est appelée avec l'argument l pour obtenir le texte de la feuille. *)
    B.text (leaf l)
  | Node n -> 
    (* Si t est un nœud (Node n), procéder comme suit : *)
    
    (* Construction du texte du nœud en fonction de la valeur de parent_branch et n.data (les données du nœud). *)
    let node_text = match Option.bind parent_branch ~f:branch with
      | None -> node n.data
      | Some b_label -> sprintf "%s - %s" b_label (node n.data)
    in
    (*
       Itérer sur la liste des branches (n.branches) du nœud en utilisant List1.map.
       Pour chaque branche (Branch b), appeler récursivement la fonction to_printbox_aux avec les paramètres appropriés pour générer la structure d'arbre du sous-arbre associé à la branche.
       Les résultats sont collectés dans une liste.
    *)
    List1.map n.branches ~f:(fun (Branch b) ->
        to_printbox_aux ~parent_branch:b.data ~node ~leaf ~branch b.tip ()
      )
    (* Convertir la liste des structures d'arbre générées pour chaque branche en une liste ordinaire. *)
    |> List1.to_list
    (* Utiliser B.tree pour créer une PrintBox.tree à partir du texte du nœud et des sous-arbres générés pour les branches. *)
    |> B.tree (B.text node_text)




(*
   Convertit un arbre en une structure d'arbre imprimable (PrintBox.tree).
   Les fonctions de rappel (node, leaf, branch) permettent de spécifier le texte à afficher pour chaque nœud, feuille et branche respectivement.
   Si une fonction de rappel n'est pas spécifiée, des valeurs par défaut sont utilisées (· pour les nœuds et les feuilles, None pour les branches).
*)
let to_printbox ?(node = fun _ -> "·") ?(leaf = fun _ -> "·") ?(branch = fun _ -> None) t =
  to_printbox_aux t ?parent_branch:None ~node ~leaf ~branch ()



(*
   Parcourt un arbre en utilisant un ordre de parcours préfixe (racine-gauche-droite).
   Les fonctions de rappel (init, node, leaf, branch) permettent de spécifier les opérations à effectuer lors de la traversée de chaque élément.
   La valeur init est la valeur initiale de l'accumulateur, qui est transmise lors de la récursion.
*)
let rec prefix_traversal t ~init ~node ~leaf ~branch =
  match t with
  | Leaf l -> leaf init l 
  | Node n ->
    (*
        Pour chaque branche du nœud, appeler récursivement la fonction prefix_traversal pour parcourir le sous-arbre associé à la branche.
        La valeur d'initialisation de l'accumulateur est mise à jour lors de chaque appel récursif.
    *)
    List1.fold
      n.branches
      ~init:(node init n.data) (* Appeler la fonction de rappel node avec l'accumulateur mis à jour et les données du nœud actuel. *)
      ~f:(fun init -> pre_branch ~init ~leaf ~node ~branch)

and pre_branch (Branch b) ~init ~node ~leaf ~branch =
  (*
      Appeler récursivement la fonction prefix_traversal pour parcourir le sous-arbre associé à la branche.
      La valeur d'initialisation de l'accumulateur est mise à jour lors de chaque appel récursif.
  *)
  prefix_traversal b.tip ~init:(branch init b.data) (* Appeler la fonction de rappel branch avec l'accumulateur mis à jour et les données de la branche actuelle. *)
    ~leaf ~node ~branch




(*
   Parcourt l'arbre par ordre préfixe (racine-gauche-droite).
   L'accumulateur init est transmis à la fonction f lors de chaque appel.
   Les fonctions de rappel branch et node sont ignorées car seules les feuilles sont traitées.
*)
let fold_leaves t ~init ~f =
  prefix_traversal t ~init
    ~branch:(fun acc _ -> acc)
    ~node:(fun acc _ -> acc)
    ~leaf:(fun acc l -> f acc l)

(*
   Récupère toutes les feuilles de l'arbre dans une liste.
   Les fonctions de rappel branch et node sont ignorées car seules les feuilles sont collectées.
*)
let leaves t =
  prefix_traversal t ~init:[]
    ~branch:(fun acc _ -> acc)
    ~node:(fun acc _ -> acc)
    ~leaf:(fun acc l -> l :: acc)
  |> List.rev


(*
  Applique des transformations aux nœuds, feuilles et branches de l'arbre.
  Les fonctions de rappel node, leaf et branch sont utilisées pour déterminer les nouvelles valeurs des éléments.
*)
let rec map t ~node ~leaf ~branch =
 match t with
 | Node n ->
   (* Appliquer la transformation node aux données du nœud *)
   Node {
     data = node n.data ;
     (* Appliquer récursivement la transformation map_branch à chaque branche *)
     branches = List1.map n.branches ~f:(map_branch ~node ~leaf ~branch) ;
   }
 | Leaf l -> Leaf (leaf l)

and map_branch (Branch b) ~node ~leaf ~branch =
 (* Appliquer la transformation branch aux données de la branche *)
 Branch {
   data = branch b.data ;
   (* Appliquer récursivement la transformation map aux sous-arbres associés à la branche *)
   tip = map b.tip ~node ~leaf ~branch ;
 }

(*
   Applique des transformations aux nœuds, feuilles et branches de deux arbres, en combinant les données correspondantes.
   Les fonctions de rappel node, leaf et branch sont utilisées pour déterminer les nouvelles valeurs des éléments.
*)
let rec map2_exn t1 t2 ~node ~leaf ~branch =
  match t1, t2 with
  | Node n1, Node n2 ->
    (* Appliquer la transformation node aux données des nœuds en les combinant *)
    Node {
      data = node n1.data n2.data ;
      (* Appliquer récursivement la transformation map_branch2_exn à chaque paire de branches correspondantes *)
      branches = List1.map2_exn n1.branches n2.branches ~f:(map_branch2_exn ~node ~leaf ~branch) ;
    }
  | Leaf l1, Leaf l2 ->
    (* Appliquer la transformation leaf aux données des feuilles en les combinant *)
    Leaf (leaf l1 l2)
  | _ -> failwith "Attempted to match node from tree to branch of other tree"

and map_branch2_exn (Branch b1) (Branch b2) ~node ~leaf ~branch =
  (* Appliquer la transformation branch aux données des branches en les combinant *)
  Branch {
    data = branch b1.data b2.data ;
    (* Appliquer récursivement la transformation map2_exn aux sous-arbres associés aux branches *)
    tip = map2_exn b1.tip b2.tip ~node ~leaf ~branch ;
  }

(*
   Propage une transformation à travers tous les éléments de l'arbre.
   Les fonctions de rappel node, leaf et branch sont utilisées pour propager la transformation
   et produire les nouvelles valeurs des éléments.
*)
let propagate t ~init ~node ~leaf ~branch =
  let rec inner state t =
    match t with
    | Node n ->
      (* Appliquer la transformation node aux données du nœud, avec un état accumulé *)
      let state', data = node state n.data in
      (* Appliquer récursivement la transformation inner_branch à chaque branche *)
      let branches = List1.map n.branches ~f:(inner_branch state') in
      Node { data ; branches }
    | Leaf l ->
      (* Appliquer la transformation leaf aux données de la feuille, avec un état accumulé *)
      Leaf (leaf state l)

  and inner_branch state (Branch b) =
    (* Appliquer la transformation branch aux données de la branche, avec un état accumulé *)
    let state', data = branch state b.data in
    (* Appliquer récursivement la transformation inner aux sous-arbres associés à la branche *)
    Branch { data ; tip = inner state' b.tip }
  in
  inner init t

(* let node_prefix_synthesis tree ~init ~f =
 *   let rec loop tree ~init =
 *     let state, children =
 *       List.fold_right tree.branches ~init:(init, []) ~f:(fun b (acc, t) ->
 *           let state, h = loop b.tip ~init:acc in
 *           state, h :: t
 *         )
 *     in
 *     let children_results = List.map children ~f:(fun n -> n.node_data) in
 *     let branches = List.map2_exn tree.branches children ~f:(fun b n -> { b with tip = n }) in
 *     let new_state, node_data = f state tree.node_data children_results in
 *     new_state, { node_data ; branches }
 *   in
 *   snd (loop tree ~init) *)

(*
   Génère un sous-arbre à partir d'un arbre existant en fonction d'un prédicat f et d'un ensemble de feuilles.
   Si une feuille de l'arbre généré n'est pas présente dans l'ensemble de feuilles, elle est supprimée.
   Les nœuds et les branches correspondants sont également supprimés si toutes leurs feuilles sont supprimées.
   La fonction de rappel f est utilisée pour mapper les feuilles à des identifiants.
*)
let rec leafset_generated_subtree t f leaves =
  match t with
  | Leaf l ->
    (* Appliquer le prédicat f à la feuille *)
    Option.bind (f l) ~f:(fun id ->
        if List.mem leaves id ~equal:String.equal then Some t
        else None
      )
  | Node n ->
    (* Filtrer les branches en appliquant récursivement leafset_generated_subtree et mapper les résultats avec les branches correspondantes *)
    List1.filter_map n.branches ~f:(fun (Branch b) ->
        leafset_generated_subtree b.tip f leaves
        |> Option.map ~f:(branch b.data)
      )
    (* Convertir la liste des branches en un arbre *)
    |> List1.of_list
    |> Option.map ~f:(node n.data)

(*
   Simplifie un nœud en supprimant les nœuds intermédiaires avec un seul enfant et en fusionnant les données des branches.
   La fonction de rappel merge_branch_data est utilisée pour fusionner les données des branches.
*)
let simplify_node_with_single_child ~merge_branch_data t =
  let rec prune_linear_root = function
    | Leaf _ as l -> l
    | Node n ->
      match n.branches with
      | Cons (Branch b, []) ->
        prune_linear_root b.tip
      | branches ->
        (* Appliquer la fonction traverse_branch à chaque branche *)
        node n.data (List1.map branches ~f:traverse_branch)
  and traverse_branch (Branch b as bb) =
    match b.tip with
    | Leaf _ -> bb
    | Node n ->
      match n.branches with
      | Cons (Branch b', []) ->
        (* Fusionner les données des branches et appliquer récursivement collapse_linear_segment *)
        collapse_linear_segment ~branch_data:[b'.data ; b.data] b'.tip
      | branches ->
        let branches = List1.map branches ~f:traverse_branch in
        let tip = node n.data branches in
        branch b.data tip
  and collapse_linear_segment ~branch_data = function
    | Leaf _ as l ->
      (* Fusionner les données des branches et créer une nouvelle branche *)
      branch (merge_branch_data branch_data) l
    | Node n ->
      match n.branches with
      | Cons (Branch b, []) ->
        (* Fusionner les données des branches et appliquer récursivement collapse_linear_segment *)
        collapse_linear_segment ~branch_data:(b.data :: branch_data) b.tip
      | branches ->
        let branches = List1.map branches ~f:traverse_branch in
        let tip = node n.data branches in
        branch (merge_branch_data branch_data) tip
  in
  prune_linear_root t

  module Simplify_node_with_single_child_tests = struct
    let n1 x = node () List1.(cons (branch () x) [])
    let n2 x y = node () List1.(cons (branch () x) [ branch () y ])
    let leaf x = leaf x
  
    (* Affiche l'arbre après avoir appliqué simplify_node_with_single_child *)
    let print t =
      simplify_node_with_single_child t ~merge_branch_data:(fun _ -> ())
      |> to_printbox ~leaf:Fn.id
      |> PrintBox_text.output stdout
  
    (* Test pour l'arbre donné *)
    let%expect_test "simplify_node_with_single_child" =
      let t =
        n2
          (n2
             (n1 (leaf "A"))
             (n2 (leaf "C") (leaf "D")))
          (leaf "E")
      in
      print t ;
      [%expect {|
      ·
      ├─·
      │ ├─A
      │ └─·
      │   ├─C
      │   └─D
      └─E |}]
  
    (* Test pour l'arbre donné *)
    let%expect_test "simplify_node_with_single_child" =
      let t = n1 (n1 (leaf "E")) in
      print t ;
      [%expect {| E |}]
  
    (* Test pour l'arbre donné *)
    let%expect_test _ =
      let t =
        n2
          (n1 (leaf "A"))
          (n1 (leaf "B"))
        |> n1
        |> n1
      in
      print t ;
      [%expect {|
        ·
        ├─A
        └─B |}]
  end
  
  (*
     Développe un arbre en appliquant des fonctions de transformation f_b, f_l et f_n aux branches, feuilles et nœuds respectivement.
     La fonction traverse_node est utilisée pour effectuer la traversée récursive de l'arbre et appliquer les transformations appropriées.
  *)
  let unfold t ~init ~branch:f_b ~leaf:f_l ~node:f_n =
    let rec traverse_node acc = function
      | Leaf l ->
        let acc', l' = f_l acc l in
        acc', leaf l'
      | Node n ->
        let acc', ni = f_n acc n.data in
        let acc'', rev_branches =
          List1.fold n.branches ~init:(acc', []) ~f:(fun (acc, branches) b ->
              let acc', b' = traverse_branch acc b in
              acc', b' :: branches
            )
        in
        acc'',
        node ni (
          rev_branches
          |> List.rev
          |> List1.of_list_exn
        )
    and traverse_branch acc (Branch b) =
      let acc', bi = f_b acc b.data in
      let acc'', tip = traverse_node acc' b.tip in
      acc'', branch bi tip
    in
    snd (traverse_node init t)
  




