open Core
open Linear_algebra

module CTMC = Phylo_ctmc
module AA = Amino_acid

let project_vector (mask : AA.t array) (v : AA.vector) =
  Vector.init (Array.length mask) ~f:(fun i -> AA.Vector.get v mask.(i))

module Evolution_model = struct

  type param = {
    stationary_distribution : AA.vector ;
    exchangeabilities : AA.matrix ;
    scale : float ;
  }

  let param_of_wag (wag : Wag.t) scale = {
    scale ;
    stationary_distribution = wag.freqs ;
    exchangeabilities = wag.rate_matrix ;
  }

  
  let stationary_distribution p = p.stationary_distribution

  let substitution_rate p i j =
    p.scale *.
    p.exchangeabilities.Amino_acid.%{i, j} *.
    p.stationary_distribution.Amino_acid.%(j)

  let rate_matrix p =
    Rate_matrix.Amino_acid.make (substitution_rate p)

  let rate_submatrix mask p =
    let n = Array.length mask in
    Rate_matrix.make n ~f:(fun i j ->
        substitution_rate p mask.(i) mask.(j)
      )

  let diag_expm m pi =
    let module V = Vector in
    let module M = Matrix in
    let sqrt_pi = V.map pi ~f:Float.sqrt in
    let sqrt_pi_inv = V.map sqrt_pi ~f:(fun v -> 1. /. v) in
    let diag_pi = M.diagm sqrt_pi in
    let diag_pi_inv = M.diagm sqrt_pi_inv in
    let m' = M.(dot diag_pi @@ dot m diag_pi_inv) in
    let (d_vec, step_transform_matrix) = M.diagonalize m' in
    Base.Staged.stage (fun t ->
        let v = V.(exp (scal_mul t d_vec)) in
        [ `Diag sqrt_pi_inv ; `Mat step_transform_matrix ; `Diag v ; `Transpose step_transform_matrix ; `Diag sqrt_pi ]
      )

  let transition_probability_submatrix mask p =
    let m = rate_submatrix mask p in
    let pi = project_vector mask p.stationary_distribution in
    diag_expm m pi

  let transition_probability_matrix p =
    let m = rate_matrix p in
    diag_expm (m :> mat) (p.stationary_distribution :> vec)

  let test_diagonal_matrix_exponential () =
    let rng = Utils.rng_of_int 21341234 in
    let stationary_distribution = AA.random_profile rng 0.5 in
    let exchangeabilities = Rate_matrix.Amino_acid.make (fun aa_i aa_j->
        let i, j = AA.(to_int aa_i, to_int aa_j) in
        if i <= j then float_of_int i *. 0.01 +. float_of_int j *. 0.001
        else float_of_int j *. 0.01 +. float_of_int i *. 0.001
      ) in
    let p = {
      stationary_distribution;
      exchangeabilities;
      scale=1.;
    } in
    let t = 100. in
    let diag_exp_matrix =
      Base.Staged.unstage (transition_probability_matrix p) t
      |> Phylo_ctmc.matrix_decomposition_reduce ~dim:AA.card
    in
    let m = rate_matrix p in
    let exp_matrix = AA.Matrix.(expm (scal_mul t m)) in
    Matrix.robust_equal ~tol:1e-10 diag_exp_matrix (exp_matrix :> mat)

  let%test "Matrix exponential through diagonalisation matches naive implementation" =
    test_diagonal_matrix_exponential ()

  let simulate_profile rng alpha =
    let theta = Array.create ~len:Amino_acid.card 0. in
    Gsl.Randist.dirichlet rng ~alpha:(Array.create ~len:Amino_acid.card alpha) ~theta ;
    AA.Vector.of_array_exn theta
end


let choose_aa p ~rng = AA.Table.(of_vector p |> choose ~rng)

(** Used to clip scale parameter value during optimisation.
    Plausible values for scale that are reported in the literature are
    generally included in [1e-2 ; 10].
    The interval implemented is roughly [2e-3 ; 20]
*)
let out_of_bound_logscale p = Float.(p > 3. || p < -6. )

(** Used to clip frequency profile parameter values during optimisation.
    This is necessary to avoid reaching frequency values too low that could
    interfere when when performing the eigen decomposition of the rate matrix.
*)
let out_of_bound_logfreq p = Float.(p > 5. || p < -20.)

(** Used to clip slope parameter values during optimisation of M4.
    This parameter is optimized in log10 scale. *)
let out_of_bound_slope_log10 p = Float.(p > 3. || p < -3. )

let tol = 0.001

let show_profile ?(mode=`Full) profile =
  match mode with
  | `Full ->
    AA.Vector.foldi profile ~init:"" ~f:(fun i acc x ->
        let acc = if AA.to_int i = 10 then acc ^ "\n" else acc in
        acc ^ sprintf "%f;\t" x)
  | `Summary threshold ->
    let str = AA.Vector.foldi profile ~init:""
        ~f:(fun i acc x ->
            if Float.(x > threshold)
            then acc ^ sprintf "[%c] %f\t" (AA.to_char i) x
            else acc
          ) in
    sprintf "%sOther AA below %f" str threshold

type simulation = (Amino_acid.t, AA.t, float) Tree.t

module Schema = struct
  type t = {
    nz : int ; (* number of non-zero AA in count table *)
    mask : AA.t array ; (* corresponding amino acids *)
  }

  let counts xs =
    AA.Table.init (fun aa ->
        List.fold ~init:0. xs ~f:(fun aa_count maybe_xaa ->
            match maybe_xaa with
            | None -> aa_count
            | Some xaa ->
              let multiplicity = IUPAC_AA.multiplicity xaa in
              let delta = 1. /. float multiplicity in
              IUPAC_AA.fold xaa ~init:aa_count
                ~f:(fun aa_count inner_aa ->
                    if AA.equal inner_aa aa
                    then aa_count +. delta
                    else aa_count
                  )
          )
      )

  let counts_from_tree_site ~leaf_state tree site =
    Tree.leaves tree
    |> List.map ~f:(leaf_state site)
    |> counts

  let make_sparse counts =
    let k = (counts : float AA.table :> _ array) in
    let idx, nz = Array.foldi k ~init:([], 0) ~f:(fun i ((assoc, nz) as acc) k_i ->
        if Float.(k_i = 0.)
        then acc
        else AA.of_int_exn i :: assoc, nz + 1
      )
    in
    if nz = 0 then Error `No_observed_AA
    else
      let mask = Array.of_list idx in
      Ok { nz ; mask }

  let make_dense () =
    let nz = AA.card in
    let mask = Array.init nz ~f:Amino_acid.of_int_exn in
    { nz ; mask }

  let make ?(mode = `sparse) counts =
    match mode with
    | `sparse -> make_sparse counts
    | `dense  -> Ok (make_dense ())

  let from_tree_site ?mode ~leaf_state tree site =
    counts_from_tree_site ~leaf_state tree site
    |> make ?mode

  let profile_guess schema counts =
    (* FIXME: are these pseudo-counts really useful? *)
    let total_counts = AA.Table.fold counts ~init:0.
        ~f:(fun acc x -> 1. +. acc +. x) in
    Array.map schema.mask ~f:(fun idx ->
        Float.log (1. +. AA.Table.get counts idx /. total_counts))

  let extract_frequencies ~offset schema param =
    let r = Array.create ~len:Amino_acid.card 0. in
    Array.iteri schema.mask ~f:(fun sparse_idx aa ->
        r.((aa :> int)) <- Float.exp param.(sparse_idx + offset)
      ) ;
    let s = Utils.array_sum r in
    AA.Vector.init (fun aa -> r.((aa :> int)) /. s)

  let aa_vector_of_sparse_vector schema v =
    let r = Amino_acid.Vector.init (fun _ -> 0.) in
    Array.iteri schema.mask ~f:(fun sparse_idx aa ->
        Amino_acid.Vector.set r aa (Vector.get v sparse_idx)
      ) ;
    r
end

let leaf_state_indicator ?schema (leaf_state:'a -> IUPAC_AA.t option) (l:'a) =
  match leaf_state l with
  | None -> Fun.const false
  | Some aa_iupac ->
    match schema with
    | Some { Schema.mask ; _ } ->
      fun i -> IUPAC_AA.mem aa_iupac mask.(i)
    | None ->
      fun i -> IUPAC_AA.mem aa_iupac (Amino_acid.of_int_exn i)
      
module Model1 = struct
  type param = { scale : float } 
(*   
  [@@deriving yojson] *)

  type _ dataset =
      Dataset : {
        exchangeabilities : Rate_matrix.Amino_acid.t ;
        stationary_distribution : AA.vector ;
        branch_length : 'branch_info -> float ;
        leaf_state : 'site -> 'leaf_info -> IUPAC_AA.t option ;
        tree : (_, 'leaf_info, 'branch_info) Tree.t ;
      } -> 'site dataset

  let dataset
      ~exchangeabilities ~stationary_distribution ~branch_length
      ~leaf_state tree =
    Dataset {
      exchangeabilities ; stationary_distribution ;
      branch_length ; leaf_state ; tree ;
    }

  let log_likelihood (Dataset d) site p =
    let root_frequencies = (d.stationary_distribution : AA.vector :> vec) in
    let p = { Evolution_model.scale = p.scale ;
              exchangeabilities = d.exchangeabilities ;
              stationary_distribution = d.stationary_distribution } in
    let transition_probabilities =
      let f = Base.Staged.unstage (Evolution_model.transition_probability_matrix p) in
      fun b -> f (d.branch_length b)
    in
    let leaf_state = leaf_state_indicator (d.leaf_state site) in
    CTMC.Ambiguous.pruning d.tree
      ~nstates:Amino_acid.card ~transition_probabilities ~leaf_state
      ~root_frequencies

  let clip f param =
    if Array.exists param ~f:out_of_bound_logscale
    then Float.infinity
    else f param

  let decode_vec p = { scale = Float.exp p.(0) }

  let inner_maximum_log_likelihood ?debug dataset site =
    let f vec =
      let p = decode_vec vec in
      -. log_likelihood dataset site p
    in
    let sample =
      let rng = Utils.rng_of_int 1239 in
      fun () -> [| Gsl.Rng.uniform rng *. 8. -. 4. |]
    in
    let ll, p_star, n_iter =
      Nelder_mead.minimize ?debug ~tol ~maxit:10_000 ~f:(clip f) ~sample () in
    -. ll, p_star, n_iter

  let maximum_log_likelihood ?debug dataset site =
    let ll, vec, _ = inner_maximum_log_likelihood ?debug dataset site in
    ll, decode_vec vec

  let branch_info_pack (type branch_info) (f : branch_info -> float)  =
    let module BI = struct
      type t = branch_info
      let length = f
    end
    in
    (module BI : Simulator.Branch_info with type t = branch_info)
end




module ModelRE08 
  (A : RE08.AlphabetBis) = struct 

  module RE08 = RE08.Make(A)
  module I = RE08.Inference


  type param = {
    stationary_distribution : A.Vector.t;
    insertion_rate : float ; 
    deletion_rate : float ; 
    bernouilli : float
  }

  (* let counts_at_site tree = 
    let module AG = RE08.A_with_gap in 
    Tree.leaves tree
    |> Core.List.filter_map ~f:(fun aa_gap -> 
      if AG.is_gap aa_gap then None 
      else Some (Some (AG.to_int aa_gap))
    )
    |> Schema.counts

  let counts_from_alignment = 
    Core.Array.fold ~init:(AA.Table.init (fun _ -> 0. )) ~f:(fun all_counts tree -> 
      let site_counts = counts_at_site tree in 
      AA.Table.mapi all_counts ~f:(fun aa c -> c +. AA.Table.get site_counts aa )
    ) *)

  let to_RE08_param param ~exchangeability = 
    { 
      I.stationary_distribution = param.stationary_distribution ;
      inserted_residues_proba = param.stationary_distribution ;
      insertion_rate = param.insertion_rate;
      deletion_rate = param.deletion_rate;
      bernouilli = param.bernouilli;
      exchangeability
    }

  let nelder_mead_init theta0 =
    let c = ref (-1) in
    fun _ ->
      incr c ;
      if !c = 0 then theta0
      else
        Array.init (Array.length theta0) ~f:(fun i ->
            theta0.(i) +. if i = !c - 1 then  -. 1. else 0.
          )

  (* let initial_param  *)
  let sigmoide x = 1.  /. (1. +. exp (-. x))
  let decode_vec vec = 
    (* [| bern ; ins ; del ; <-- stat dist --> |] *)
    (* let bernouilli = sigmoide vec.(0) in *)
    let bernouilli = sigmoide vec.(0) in
    let insertion_rate = exp vec.(1) in 
    let deletion_rate = exp vec.(2) in
    (* let stationary_distribution = Schema.extract_frequencies ~offset:3 schema vec in *)
    let stationary_distribution = 
      A.Vector.init (fun i -> exp vec.(A.to_int i + 3))
      |> A.Vector.normalize 
    in { 
      stationary_distribution ;
      insertion_rate ; 
      deletion_rate ; 
      bernouilli
    }

  
  let initial_param () : float array = 
    Array.concat [ 
      [| log 0.1; log 0.9; log 0.9 ; |];
      [| log 0.25; log 0.25; log 0.25; log 0.25;|]
    ]
    
  let default_exchangeability = A.Matrix.init (fun i j -> if A.equal i  j then -. (float A.card) else 1.)

  let inner_maximum_log_likelihood ?debug ?(exchangeability=default_exchangeability) trees =
    (* let counts = counts_from_alignment trees in 
    let schema = match Schema.make ~mode:`sparse counts with 
      | Ok schema -> schema 
      | Error `No_observed_AA -> 
        failwith "FIXME: API is not adapted to this use case. \ 
                  This should never happen" in *)
    let f vec =
      let param = decode_vec vec in 
      let param_RE08 = to_RE08_param param ~exchangeability:exchangeability in 
      let res = I.log_likelihood_alignment param_RE08 trees in 
      -. res
       
    in
    let sample = nelder_mead_init (initial_param ()) in
    let ll, p_star, n_iter =
      Nelder_mead.minimize ?debug ~tol ~maxit:60_000 ~f:f ~sample () in
      (* Format.printf "ll : %f\n" ll; *)
    -. ll, p_star, n_iter
  
  let maximum_log_likelihood ?debug ?exchangeability trees = 
    let loglkh, param_vec, _ = inner_maximum_log_likelihood ?debug ?exchangeability trees in 
    loglkh, decode_vec param_vec

end


module TestSimInference = struct 

  module A = Nucleotide
  module M = RE08.Make(A)
  module I = M.Inference
  module Model = ModelRE08(A)
(* 
let newick_tree_to_tree_extended (newick_tree : Newick.Tree_repr.t) : (RE08.node_info, M.A_with_gap.t, RE08.branch_info) Tree.t = 
  let open Tree in 
  let rec aux node_id = function 
    | Newick.Tree_repr.Tree tree-> 
      (match tree with 
      | Leaf l -> Tree.Leaf { RE08.seq_id = l  }
      | Node data -> 
        Node data)
    | Branch _ -> 
      failwith "Internal error: newick_tree_to_tree_extended: branch not supported"
  in aux newick_tree *)
let simulate ?verbose ?bernoulli ?size_ancestral_seq tree_extended ~param ~rng = 
  let open M.Make_simulator in
  let rate_matrix = rate_matrix param in 
  (* let q_eps = Linear_algebra.Matrix.(expm (scal_mul 1. rate_matrix)) in *)
  (* let _ = Linear_algebra.Matrix.pp Format.std_formatter q_eps in *)
  let frq_residues = param.stationary_distribution in 
  match bernoulli, size_ancestral_seq with
  | None, None -> failwith "Internal error: simulate: need to specify either bernoulli or size_ancestral_seq"
  | Some _, Some _ -> failwith "Internal error: simulate: you can't specify both bernoulli and size_ancestral_seq"
  | Some bernoulli, None -> 
    let terate = terate_create rate_matrix param.insertion_rate param.deletion_rate ~bernoulli frq_residues false in 
    let msa, nb_insertions, nb_deletions, nb_substitutions = generate_alignment ?verbose rng tree_extended terate in
    let alignement = MSA.to_alignment msa in 
    alignement, msa, nb_insertions, nb_deletions, nb_substitutions
  | None, Some size_ancestral_seq ->
    if size_ancestral_seq <= 0 then failwith "Internal error: simulate: size_ancestral_seq must be positive"
    else
    let terate = terate_create rate_matrix param.insertion_rate param.deletion_rate frq_residues true ~size_ancestral_seq in 
    let msa, nb_insertions, nb_deletions, nb_substitutions = generate_alignment ?verbose rng tree_extended terate in
    let alignement = MSA.to_alignment msa in 
    alignement, msa, nb_insertions, nb_deletions, nb_substitutions
  (* let terate = terate_create rate_matrix param.insertion_rate param.deletion_rate bernoulli frq_residues false in 
  let msa = generate_alignment rng tree_extended terate in *)

  (* let alignement = MSA.to_alignment msa in 
  alignement, msa *)
  (* let _ = Alignment.Fasta.to_file alignement "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/alignement.txt" in  *)

let inference trees = 
  let loglkh, param = Model.maximum_log_likelihood trees in 
  (* do stuff *)
  let { Model.stationary_distribution ; insertion_rate ; deletion_rate ; bernouilli } = param in
  Format.printf "[APRES SIMULATION] bernouilli : %f\n" bernouilli;
  Format.printf "[APRES SIMULATION] insertion_rate : %f\n" insertion_rate;
  Format.printf "[APRES SIMULATION] deletion_rate : %f\n" deletion_rate;
  A.Vector.pp Format.std_formatter stationary_distribution;
  Format.printf "\n";
  Format.printf "[APRES SIMULATION] loglkh : %f\n"  loglkh;
  Format.printf "[APRES SIMULATION] likelihood : %f\n" (exp (loglkh));
  (* Format.printf "\n"; *)
  loglkh, param

let%expect_test "simulation -> inference (simple tree with 3 taxa)" = 
  let tree_extended = {
    Tree.tree = Node {
      data = { RE08.seq_id = 0 };
      branches = List1.of_list_exn [
        Tree.Branch { data = {RE08.length = 1.}; tip = Leaf {RE08.seq_id = 1} };
        Branch { data = {length = 1.}; tip = Leaf {seq_id = 2} }
      ]
    };
    node_count = 3
  } in 

  let rng = Gsl.Rng.make Gsl.Rng.FISHMAN18 in   
  let open M.Make_simulator in
  let stationary_distribution = 
    A.Vector.of_array_exn [| 0.1; 0.2 ; 0.3 ; 0.4 |]
    |> A.Vector.normalize in
  let p = 
    let exchangeability = A.Matrix.init (fun i j -> if A.equal i j then -. (float A.card) else 1.) in
    let insertion_rate = 0.6 in 
    let deletion_rate = 0.3 in 
    let inserted_residues_proba = stationary_distribution in
    {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba }
  in
  let bernoulli = 0.01 in
  let _, msa, _ ,_ ,_ = simulate tree_extended ~param:p ~bernoulli:bernoulli ~rng:rng in 
  let trees_array = MSA.get_trees_of_msa msa ~tree:tree_extended.tree in 

  let true_param = { 
    I.exchangeability = p.exchangeability ; 
    stationary_distribution = p.stationary_distribution ; 
    bernouilli = bernoulli ; 
    deletion_rate = p.deletion_rate ; 
    insertion_rate = p.insertion_rate ; 
    inserted_residues_proba = p.inserted_residues_proba ;
  } in 
  (* let true_param, trees_array = simulate tree_extended p bernoulli rng in  *)
  let _simulation_likelihood = I.log_likelihood_alignment true_param trees_array in
  (* print true likelihood *)
  Format.printf "[SIMULATION] I.log_likelihood_alignment : %f\n" _simulation_likelihood;
  Format.printf "[SIMULATION] True likelihood : exp(%f) = %f\n" _simulation_likelihood (exp _simulation_likelihood);
  (* inference *)
  let _, _ = inference trees_array in 
  [%expect {|
    MSA generated. Length : 773

    [SIMULATION] I.log_likelihood_alignment : -3188.895096
    [SIMULATION] True likelihood : exp(-3188.895096) = 0.000000
    [APRES SIMULATION] bernouilli : 0.996072
    [APRES SIMULATION] insertion_rate : 0.775633
    [APRES SIMULATION] deletion_rate : 0.140613

    0.109941
    0.210154
    0.295503
    0.384401
    [APRES SIMULATION] loglkh : -2039.904032
    [APRES SIMULATION] likelihood : 0.000000 |}] 
(* 
  let%expect_test "simulation -> inference (tree from newick)" = 
    let open M.Make_simulator in
    let stationary_distribution = 

      A.Vector.of_array_exn [| 0.263704;0.239449;0.223828;0.273018;|]
      |> A.Vector.normalize in
    let p = 
      let exchangeability = A.Matrix.init (fun i j -> if A.equal i j then -. (float A.card) else 10000.) in
      let insertion_rate = 0.203764 in 
      let deletion_rate = 0.624647 in 
      let inserted_residues_proba = stationary_distribution in
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba }
    in
    let bernoulli = 0.998934 in
    let alignement = Alignment.Fasta.from_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/lib/align.fasta" in
    (match alignement with
    | Error _ -> failwith "error"
    | Ok alignement ->
      let msa, seqs_names = MSA.of_alignement_RE08 alignement in
      let t = Newick.from_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/lib/tree_a_eval.newick" in
        match t with 
        | Error _ -> failwith "error"
        | Ok t ->
          let nb_nodes, tree =
          let cpt_nodes = ref 0 in
          let node_func {Newick.Tree_repr.name; _} = 
              incr cpt_nodes; 
              (match name with
              | Some name ->
                (* On affiche tous les numéro dans la table de hash seqs_names *)
                Format.printf "%s : %d\n" name !cpt_nodes;

                (* On récupère le numéro de la séquence via la table de hash seqs_names *)
                let seq_id = 
                  match Hashtbl.find seqs_names name with
                  | Some seq_id -> seq_id
                  | None -> failwith "Node name not found in seqs_names"
                in
                  {RE08.seq_id}
                | None -> {RE08.seq_id = -1}) (* Les noeuds internes n'ont pas de nom, on leur attribue un numéro négatif: leur numéro n'a pas d'importance *);
              in 
          let tree = Newick.Tree_repr.of_ast t 
          |> Newick.Tree_repr.with_inner_tree 
            ~f:(Tree.map
              ~node:node_func
              ~branch:(fun f -> match f with | Some length -> {RE08.length}
                  | None -> failwith "Branch without length") 
              ~leaf:node_func
              ) in 
          !cpt_nodes, tree
        in 

      let tree_extended = { Tree.tree ; node_count = nb_nodes} in 
      let trees_array = MSA.get_trees_of_msa msa ~tree:tree_extended.tree in 
      let true_param = { 
        I.exchangeability = p.exchangeability ; 
        stationary_distribution = p.stationary_distribution ; 
        bernouilli = bernoulli ; 
        deletion_rate = p.deletion_rate ; 
        insertion_rate = p.insertion_rate ; 
        inserted_residues_proba = p.inserted_residues_proba ;
      } in 
      (* let alpha = 0.601499 in
      let beta = 0.398501 in *)
      (* let f84_matrix = I.f84_rate_matrix_nucleotide_maker true_param ~alpha ~beta in *)
      (* let true_param, trees_array = simulate tree_extended p bernoulli rng in  *)
      let _simulation_likelihood = I.log_likelihood_alignment  true_param trees_array in
      (* print true likelihood *)
      Format.printf "[SIMULATION] I.log_likelihood_alignment : %f\n" _simulation_likelihood;
      Format.printf "[SIMULATION] True likelihood : exp(%f) = %f\n" _simulation_likelihood (exp _simulation_likelihood);
    );
    [%expect {||}]  *)
  
(* let%expect_test "simulation -> inference (tree from newick)" = 
    let open M.Make_simulator in
    let stationary_distribution = 

      A.Vector.of_array_exn [| 0.247616;0.249392;0.258833;0.244158;|]
      |> A.Vector.normalize in
    let p = 
      let exchangeability = A.Matrix.init (fun i j -> if A.equal i j then -. (float A.card) else 1.) in
      let insertion_rate = 0.408437 in 
      let deletion_rate = 0.591563 in 
      let inserted_residues_proba = stationary_distribution in
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba }
    in
    let bernoulli = 0.998973 in
    let alignement = Alignment.Fasta.from_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/lib/align.fasta" in
    (match alignement with
    | Error _ -> failwith "error"
    | Ok alignement ->
      let msa, seqs_names = MSA.of_alignement_RE08 alignement in
      let t = Newick.from_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/lib/tree_a_eval.newick" in
      match t with 
      | Error _ -> failwith "error"
      | Ok t ->
        let nb_nodes, tree =
          let cpt_nodes = ref 0 in
          let node_func {Newick.Tree_repr.name; _} = 
              incr cpt_nodes; 
              (match name with
              | Some name ->
                (* On récupère le numéro de la séquence via la table de hash seqs_names *)
                let seq_id = 
                  match Hashtbl.find seqs_names name with
                  | Some seq_id -> seq_id
                  | None -> failwith "Node name not found in seqs_names"
                in
                  {RE08.seq_id}
                | None -> {RE08.seq_id = -1}) (* Les noeuds internes n'ont pas de nom, on leur attribue un numéro négatif: leur numéro n'a pas d'importance *);
              in 
          let tree = Newick.Tree_repr.of_ast t 
          |> Newick.Tree_repr.with_inner_tree 
            ~f:(Tree.map
              ~node:node_func
              ~branch:(fun f -> match f with | Some length -> Format.printf "length: %f\n" length ;{RE08.length}
                  | None -> failwith "Branch without length") 
              ~leaf:node_func
              ) in 
          !cpt_nodes, tree
        in 

      let tree_extended = { Tree.tree ; node_count = nb_nodes} in 
      let _ = MSA.pp msa in 
      let trees_array = MSA.get_trees_of_msa msa ~tree:tree_extended.tree in 

      let true_param = { 
        I.exchangeability = p.exchangeability ; 
        stationary_distribution = p.stationary_distribution ; 
        bernouilli = bernoulli ; 
        deletion_rate = p.deletion_rate ; 
        insertion_rate = p.insertion_rate ; 
        inserted_residues_proba = p.inserted_residues_proba ;
      } in 
      let _simulation_likelihood = I.log_likelihood_alignment true_param trees_array in
      (* print true likelihood *)
      Format.printf "[SIMULATION] I.log_likelihood_alignment : %f\n" _simulation_likelihood;
      Format.printf "[SIMULATION] True likelihood : exp(%f) = %f\n" _simulation_likelihood (exp _simulation_likelihood);
      (* inference *)
      inference trees_array;
      )
  ;
  (* (* execute the external program of Rivas & Eddy to compare likelihood*)
      let _ = Sys_unix.command "/home/corentin/Téléchargements/rivas-et-eddy/erate-supplement-R2/src/phylip3.66-erate/exe/dnaml-erate < /home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/alignement.phy " in
      let lines = In_channel.read_lines "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/outfile" in 
      Format.printf "%s\n" (List.nth_exn lines 57); *)
      [%expect {||}]  *)
  

      
  type data_boxplot = {
    mutable sequence_size :  string ref list;
    mutable error_rate: float ref list;
  }

  let write_data_to_file filename data message = 
    let oc = Core.Out_channel.create filename in
    fprintf oc "{\n";
    fprintf oc "    'Taille de séquence': [";
    Core.List.iter ~f:(fun cat -> fprintf oc "'%s', " !cat) data.sequence_size;
    fprintf oc "],\n";
    fprintf oc "    '%s': [" message;
    Core.List.iter ~f:(fun value -> fprintf oc "%f, " !value) data.error_rate;
    fprintf oc "]\n";
    fprintf oc "}";
    Core.Out_channel.close oc

  let write_point_in_file filename x_array y_array = 
    let oc = Core.Out_channel.create filename in
    fprintf oc "{\n";
    fprintf oc "    'x': [";
    Core.List.iter ~f:(fun x -> fprintf oc "%f, " x) x_array;
    fprintf oc "],\n";
    fprintf oc "    'y': [";
    Core.List.iter ~f:(fun y -> fprintf oc "%f, " y) y_array;
    fprintf oc "]\n";
    fprintf oc "}";
    Core.Out_channel.close oc



  let test_du_simulateur () = 
    
    let open M.Make_simulator in
    let stationary_distribution = 
      A.Vector.of_array_exn [| 0.25;0.25;0.25;0.25|]
      |> A.Vector.normalize in
    let rng = Gsl.Rng.make Gsl.Rng.FISHMAN18 in 
    let x_list_insertion = ref [] in
    let x_list_deletion = ref [] in
    let x_list_subst = ref [] in
    let y_list_insertion = ref [] in
    let y_list_deletion = ref [] in
    let y_list_subst = ref [] in
    let p = 
      let exchangeability = A.Matrix.init (fun i j -> if A.equal i j then -. (float A.card) else 10000.) in
      let insertion_rate = 0.2 in 
      let deletion_rate = 0.6 in 
      let inserted_residues_proba = stationary_distribution in
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba }
    in
    (* let bernoulli = 0.99 in *)
    let t = Newick.from_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/lib/tree_a_eval.newick" in
    match t with 
    | Error _ -> failwith "error"
    | Ok t ->
      let nb_nodes, tree =
      let cpt_nodes = ref 0 in
      let node_func {Newick.Tree_repr.name; _} = 
          incr cpt_nodes; 
          (match name with
          | Some _ ->
            {RE08.seq_id = !cpt_nodes - 1}
          | None -> {RE08.seq_id = !cpt_nodes - 1}) (* Les noeuds internes n'ont pas de nom, on leur attribue un numéro négatif: leur numéro n'a pas d'importance *);
          in 
      let tree = Newick.Tree_repr.of_ast t 
      |> Newick.Tree_repr.with_inner_tree 
        ~f:(Tree.map
          ~node:node_func
          ~branch:(fun f -> match f with | Some length -> {RE08.length}
              | None -> failwith "Branch without length") 
          ~leaf:node_func
          ) in 
      !cpt_nodes, tree
    in 

    let tree_extended = { Tree.tree ; node_count = nb_nodes} in
    let x = ref 0 in
    for _ = 0 to 0999 do 
      (* let simulate ?verbose ?bernoulli ?size_ancestral_seq tree_extended ~param ~rng = *)
      let _, _, nb_insertion, nb_deletion, nb_substitution = simulate ~verbose:true tree_extended ~param:p ~size_ancestral_seq:100 ~rng:rng in
      Format.printf "nb_insertion : %d\n" nb_insertion;
      Format.printf "nb_deletion : %d\n" nb_deletion;
      Format.printf "nb_substitution : %d\n" nb_substitution;
      x_list_insertion := float_of_int !x :: !x_list_insertion;
      y_list_insertion := float_of_int nb_insertion :: !y_list_insertion;
      x_list_deletion := float_of_int !x :: !x_list_deletion;
      y_list_deletion := float_of_int nb_deletion :: !y_list_deletion;
      x_list_subst := float_of_int !x :: !x_list_subst;
      y_list_subst := float_of_int nb_substitution :: !y_list_subst;
      incr x;
    done;
    write_point_in_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/insertion.txt" !x_list_insertion !y_list_insertion;
    write_point_in_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/deletion.txt" !x_list_deletion !y_list_deletion;
    write_point_in_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/substitution.txt" !x_list_subst !y_list_subst

 let main () =
  let write_data_to_file filename data message = 
    let oc = Core.Out_channel.create filename in
    fprintf oc "{\n";
    fprintf oc "    'Taille de séquence': [";
    Core.List.iter ~f:(fun cat -> fprintf oc "'%s', " !cat) data.sequence_size;
    fprintf oc "],\n";
    fprintf oc "    '%s': [" message;
    Core.List.iter ~f:(fun value -> fprintf oc "%f, " !value) data.error_rate;
    fprintf oc "]\n";
    fprintf oc "}";
    Core.Out_channel.close oc
  in
  let open M.Make_simulator in
  let seq_lengths_test = [|8 ; 32 ; (*1000; 4000;  5000; 10000*)|] in
  (* On itère sur les longueurs de séquences à simuler *)
  let data_error_rate_insertion = {sequence_size = []; error_rate = []} in
  let data_error_rate_deletion = {sequence_size = []; error_rate = []} in
  let data_error_rate_compA = {sequence_size = []; error_rate = []} in
  let data_error_rate_compC = {sequence_size = []; error_rate = []} in
  let data_error_rate_compG = {sequence_size = []; error_rate = []} in
  let data_error_rate_compT = {sequence_size = []; error_rate = []} in
  let rng = Gsl.Rng.make Gsl.Rng.FISHMAN18 in 
  for i = 0 to Array.length seq_lengths_test - 1 do
    let seq_length_string = string_of_int seq_lengths_test.(i) in
    let min_seq = ref 999999999 in
    let max_seq = ref (-999999999) in
    for _ = 0 to 0 do
      
      (let stationary_distribution = A.Vector.init (fun i -> (1. +. float (A.card - (A.to_int i)))) |> A.Vector.normalize in 
      let p = 
        let exchangeability = A.Matrix.init (fun i j -> if A.equal i j then -. (float A.card) else 0.1) in
        let insertion_rate = 10. in 
        let deletion_rate = 5. in 
        let inserted_residues_proba = A.Vector.normalize (A.Vector.init (fun i -> (exp (20. *. (1. +. float (A.to_int i)))))) in 
        {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba;}
      in   
        let _ = Format.printf "stationary_distribution : \n" in
        let _ = A.Vector.pp Format.std_formatter p.stationary_distribution in
        let bernoulli = 0.998973 in
        let seq_length = seq_lengths_test.(i) in
        let t = Newick.from_file "/home/corentin/Bureau/Cours/corrections-ensl/L3/S2/Stage/phylogenetics/lib/tree_a_eval.newick" in
        match t with 
        | Error _ -> failwith "error"
        | Ok t ->
          let nb_nodes, tree =
            let cpt_nodes = ref 0 in
            let node_func _ = 
                incr cpt_nodes; 
                {RE08.seq_id = !cpt_nodes - 1} 
            in

            let tree = Newick.Tree_repr.of_ast t 
            |> Newick.Tree_repr.with_inner_tree 
              ~f:(Tree.map
                ~node:node_func
                ~branch:(fun f -> match f with | Some length -> {RE08.length}
                    | None -> failwith "Branch without length") 
                ~leaf:node_func
                ) in 
          !cpt_nodes, tree
        in 

      let tree_extended = { Tree.tree ; node_count = nb_nodes} in 
      let _, msa, _, _, _ = simulate tree_extended ~param:p ~size_ancestral_seq:seq_length ~rng:rng in 
      min_seq := if MSA.ncols msa < !min_seq then MSA.ncols msa else !min_seq;
      max_seq := if MSA.ncols msa > !max_seq then MSA.ncols msa else !max_seq;
      let trees_array = MSA.get_trees_of_msa msa ~tree:tree_extended.tree in 
      let true_param = { 
        I.exchangeability = p.exchangeability ; 
        stationary_distribution = p.stationary_distribution ; 
        bernouilli = bernoulli ; 
        deletion_rate = p.deletion_rate ; 
        insertion_rate = p.insertion_rate ; 
        inserted_residues_proba = p.inserted_residues_proba ;
      } in 
      (* let true_param, trees_array = simulate tree_extended p bernoulli rng in  *)
      let _simulation_likelihood = I.log_likelihood_alignment true_param trees_array in
      (* print true likelihood *)
      Format.printf "[SIMULATION] I.log_likelihood_alignment : %f\n" _simulation_likelihood;
      Format.printf "[SIMULATION] True likelihood : exp(%f) = %f\n" _simulation_likelihood (exp _simulation_likelihood);
      (* inference *)
      let _, param = inference trees_array in 
      (* let _ = assert (Float.(>=.) loglkh_after_sim _simulation_likelihood) in *)
      (* let { Model.stationary_distribution ; insertion_rate ; deletion_rate ; bernouilli } = param in *)
      let error_insertion_rate = p.insertion_rate -. param.insertion_rate in
      let error_deletion_rate = p.deletion_rate -. param.deletion_rate in
      let error_stationnary_dist_compA = (A.Vector.get p.stationary_distribution (A.of_int_exn 0)) -. (A.Vector.get param.stationary_distribution (A.of_int_exn 0)) in
      let error_stationnary_dist_compC = (A.Vector.get p.stationary_distribution (A.of_int_exn 1)) -. (A.Vector.get param.stationary_distribution (A.of_int_exn 1)) in
      let error_stationnary_dist_compG = (A.Vector.get p.stationary_distribution (A.of_int_exn 2)) -. (A.Vector.get param.stationary_distribution (A.of_int_exn 2)) in
      let error_stationnary_dist_compT = (A.Vector.get p.stationary_distribution (A.of_int_exn 3)) -. (A.Vector.get param.stationary_distribution (A.of_int_exn 3)) in
      
      data_error_rate_insertion.error_rate <- List.append [ref error_insertion_rate] data_error_rate_insertion.error_rate ;
      data_error_rate_insertion.sequence_size <- List.append [ref seq_length_string] data_error_rate_insertion.sequence_size;

      data_error_rate_deletion.error_rate <- List.append [ref error_deletion_rate] data_error_rate_deletion.error_rate;
      data_error_rate_deletion.sequence_size <- List.append [ref seq_length_string] data_error_rate_deletion.sequence_size;
      
      data_error_rate_compA.error_rate <- List.append [ref error_stationnary_dist_compA] data_error_rate_compA.error_rate;
      data_error_rate_compA.sequence_size <- List.append [ref seq_length_string] data_error_rate_compA.sequence_size;

      data_error_rate_compC.error_rate <- List.append [ref error_stationnary_dist_compC] data_error_rate_compC.error_rate;
      data_error_rate_compC.sequence_size <- List.append [ref seq_length_string] data_error_rate_compC.sequence_size;

      data_error_rate_compG.error_rate <- List.append [ref error_stationnary_dist_compG] data_error_rate_compG.error_rate;
      data_error_rate_compG.sequence_size <- List.append [ref seq_length_string] data_error_rate_compG.sequence_size;

      data_error_rate_compT.error_rate <- List.append [ref error_stationnary_dist_compT] data_error_rate_compT.error_rate;
      data_error_rate_compT.sequence_size <- List.append [ref seq_length_string] data_error_rate_compT.sequence_size;
      )
    done;
    assert (List.length data_error_rate_insertion.error_rate = List.length data_error_rate_insertion.sequence_size);
      
    (* On remplace la taille des séquences par la taille minimale et maximale des séquences générées *)
    let new_seq_length_string = string_of_int !min_seq ^ " - " ^ string_of_int !max_seq in 
    let _ = Format.printf "new_seq_length_string : %s\n" new_seq_length_string in
    (* On affiche les données dans le terminal *)

    let _ = Core.List.iteri (List.rev data_error_rate_insertion.sequence_size) ~f:(fun _ elt -> Format.printf "sequence_size : %s\n" !elt) in
    (* On itère sur la liste des tailles de séquences générées *)
    data_error_rate_insertion.sequence_size <- Core.List.fold_right data_error_rate_insertion.sequence_size ~init:[] ~f:(fun elt acc -> if String.equal !elt seq_length_string && !min_seq <> !max_seq then ref new_seq_length_string :: acc else elt :: acc);
    data_error_rate_deletion.sequence_size <- Core.List.fold_right data_error_rate_deletion.sequence_size ~init:[] ~f:(fun elt acc -> if String.equal !elt seq_length_string && !min_seq <> !max_seq then ref new_seq_length_string :: acc else elt :: acc);
    data_error_rate_compA.sequence_size <- Core.List.fold_right data_error_rate_compA.sequence_size ~init:[] ~f:(fun elt acc -> if String.equal !elt seq_length_string && !min_seq <> !max_seq then ref new_seq_length_string :: acc else elt :: acc);
    data_error_rate_compC.sequence_size <- Core.List.fold_right data_error_rate_compC.sequence_size ~init:[] ~f:(fun elt acc -> if String.equal !elt seq_length_string && !min_seq <> !max_seq then ref new_seq_length_string :: acc else elt :: acc);
    data_error_rate_compG.sequence_size <- Core.List.fold_right data_error_rate_compG.sequence_size ~init:[] ~f:(fun elt acc -> if String.equal !elt seq_length_string && !min_seq <> !max_seq then ref new_seq_length_string :: acc else elt :: acc);
    data_error_rate_compT.sequence_size <- Core.List.fold_right data_error_rate_compT.sequence_size ~init:[] ~f:(fun elt acc -> if String.equal !elt seq_length_string && !min_seq <> !max_seq then ref new_seq_length_string :: acc else elt :: acc);
  done;

  data_error_rate_insertion.sequence_size <- List.rev data_error_rate_insertion.sequence_size;
  data_error_rate_insertion.error_rate <- List.rev data_error_rate_insertion.error_rate;

  data_error_rate_deletion.sequence_size <- List.rev data_error_rate_deletion.sequence_size;
  data_error_rate_deletion.error_rate <- List.rev data_error_rate_deletion.error_rate;

  data_error_rate_compA.sequence_size <- List.rev data_error_rate_compA.sequence_size;
  data_error_rate_compA.error_rate <- List.rev data_error_rate_compA.error_rate;

  data_error_rate_compC.sequence_size <- List.rev data_error_rate_compC.sequence_size;
  data_error_rate_compC.error_rate <- List.rev data_error_rate_compC.error_rate;

  data_error_rate_compG.sequence_size <- List.rev data_error_rate_compG.sequence_size;
  data_error_rate_compG.error_rate <- List.rev data_error_rate_compG.error_rate;

  data_error_rate_compT.sequence_size <- List.rev data_error_rate_compT.sequence_size;
  data_error_rate_compT.error_rate <- List.rev data_error_rate_compT.error_rate;

  

  assert (List.length data_error_rate_insertion.error_rate = List.length data_error_rate_insertion.sequence_size);
  write_data_to_file "data_error_rate_insertion.json" data_error_rate_insertion "Taux d\\'erreur d\\'insertion";
  write_data_to_file "data_error_rate_deletion.json" data_error_rate_deletion "Taux d\\'erreur de délétion";
  write_data_to_file "data_error_rate_compA.json" data_error_rate_compA "Taux d\\'erreur sur la distribution stationnaire (composante A)";
  write_data_to_file "data_error_rate_compC.json" data_error_rate_compC "Taux d\\'erreur sur la distribution stationnaire (composante C)";
  write_data_to_file "data_error_rate_compG.json" data_error_rate_compG "Taux d\\'erreur sur la distribution stationnaire (composante G)";
  write_data_to_file "data_error_rate_compT.json" data_error_rate_compT "Taux d\\'erreur sur la distribution stationnaire (composante T)";
  
end
