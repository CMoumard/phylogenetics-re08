(** A single DNA base *)

include Alphabet.S_int

val a : t
(** The base 'A'. *)

val c : t
(** The base 'C'. *)

val g : t
(** The base 'G'. *)

val t : t
(** The base 'T'. *)

(** Creates a DNA base from a char (case insensitive).
    Raises invalid_arg in case of an incorrect char parameter. *)
val of_char_exn : char -> t

(** Returns a single capital character representing the base. *)
val to_char : t -> char

val transversion : t -> t -> bool
(** Checks if two DNA bases are a transversion.
    Returns true if the bases are a transversion, false otherwise.
    A transversion occurs when the two bases are different and belong to different purine/pyrimidine groups. *)

type repr = A | C | G | T

val inspect : t -> repr
(** Returns the representation of a DNA base as a variant type.
    The representation can be 'A', 'C', 'G', or 'T'. *)

(** Examples: *)

(** [of_char_exn 'A'] returns [a]. *)
(** [of_char_exn 'a'] returns [a]. *)
(** [of_char_exn 'C'] returns [c]. *)
(** [of_char_exn 'g'] returns [g]. *)
(** [of_char_exn 'X'] raises [Invalid_argument]. *)
(** [to_char a] returns ['A']. *)
(** [to_char c] returns ['C']. *)
(** [to_char g] returns ['G']. *)
(** [to_char t] returns ['T']. *)
(** [transversion a g] returns [false]. *)
(** [transversion c t] returns [false]. *)
(** [transversion a c] returns [true]. *)
(** [inspect a] returns [A]. *)
(** [inspect t] returns [T]. *)
