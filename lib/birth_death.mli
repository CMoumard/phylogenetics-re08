(** Provides functionality for simulating birth-death 
    processes and generating tree structures based on birth and death rates. 

    It includes the following functions:
    
    - [make] : Creates a birth-death parameter object with the given birth rate 
      and death rate. It raises an exception if either the birth rate or the 
      death rate is negative.

    - [simulation] : Simulates a birth-death process using the specified 
      parameters and returns a tree structure representing the process. 
      Each node in the tree corresponds to a birth or death event, and the 
      tree is represented using the [Tree.branch] type.

    - [age_ntaxa_simulation] : Simulates a birth-death process conditioned on 
      the age of the Most Recent Common Ancestor (MRCA) and the number of taxa. 
      It returns a tree structure representing the process, where each node has 
      an associated birth or death event time. The simulation is based on an 
      algorithm adapted from the TESS R package and raises an exception if 
      the death rate is greater than the birth rate.

    These functions facilitate the simulation and analysis of birth-death 
    processes, offering the ability to study evolutionary dynamics and 
    generate tree structures that capture the branching patterns over time.
  *)

type t = private {
  birth_rate : float ;
  death_rate : float ;
}

(** Type representing birth and death rates. *)

val make : birth_rate:float -> death_rate:float -> t
(** [make ~birth_rate ~death_rate] creates a birth death parameter object
    with the given birth rate and death rate. It raises an [Invalid_arg]
    exception if either the birth rate or the death rate is negative. *)

val simulation :
  t ->
  Gsl.Rng.t ->
  time:float ->
  (int, int, float) Tree.branch
(** [simulation p rng ~time] simulates a birth death process using the parameters
    [p] and the random number generator [rng] for the given [time] duration.
    It returns a tree structure representing the process, where each node has
    an associated birth or death event time. The tree structure is represented
    using the [Tree.branch] type from the ID_monad module. *)

val age_ntaxa_simulation :
  ?sampling_probability:float ->
  t ->
  Gsl.Rng.t ->
  age:float ->
  ntaxa:int ->
  (unit, int, float) Tree.t
(** [age_ntaxa_simulation ?sampling_probability p rng ~age ~ntaxa] simulates a birth death
    process of parameters [p] using the random generator [rng]. The simulation is conditioned
    on the age of the Most Recent Common Ancestor (MRCA) being [age] and having [ntaxa] leaves.
    It returns a tree structure representing the process, where each node has an associated
    birth or death event time. The tree structure is represented using the [Tree.t] type from
    the ID_monad module. It raises an [Invalid_arg] exception if the death rate is greater than
    the birth rate. The simulation algorithm is adapted from the TESS R package and is based on
    the underlying algorithm described in the paper "Inferring Speciation and Extinction Rates
    under Different Sampling Schemes" by Sebastian Höhna et al. *)
