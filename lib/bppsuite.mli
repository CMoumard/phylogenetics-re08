(** This file provides types to represent different alphabets such as [DNA], 
    [RNA], proteins, etc., as well as models like [JC69] and [K80]. It also offers 
    functions to convert these values into string representations. 
    Additionally, it includes a Cmd module that facilitates the generation of 
    command lines for executing specific commands, making it easier to 
    integrate and automate analyses and simulations. *)

type alphabet =
  | DNA
  | RNA
  | Protein
  | Binary
  | Word of { letter : [`DNA | `RNA | `Protein] ;
              length : int }
  | Codon of { letter : [`DNA | `RNA] }
  (** Type representing different alphabets used in bioinformatics.
      - [DNA]: Represents the DNA alphabet.
      - [RNA]: Represents the RNA alphabet.
      - [Protein]: Represents the protein alphabet.
      - [Binary]: Represents a binary alphabet.
      - [Word { letter ; length }]: Represents a custom word alphabet with a specific letter type and length. The letter field specifies the type of letter that makes up the word, with the possible options [DNA | RNA | Protein]. For example, if letter is DNA, it means that the word is composed of letters of the DNA alphabet (A, C, G, T).
        The length field indicates the length of the word. It specifies how many letters are present in each word of the custom alphabet. For example, if length is 3, it means that each custom alphabet word is made up of 3 letters.

      - [Codon { letter }]: Represents a codon alphabet with either DNA or RNA letters. *)

val string_of_letter : [< `DNA | `RNA | `Protein] -> string
(** [string_of_letter letter] converts a letter of the DNA, RNA, or Protein alphabet
    to its string representation. *)

val string_of_alphabet : alphabet -> string
(** [string_of_alphabet alphabet] converts an alphabet value to its string representation.
    The resulting string represents the type of alphabet along with any additional information
    specific to the alphabet variant. 

    For example:

    - For the Binary alphabet, the string representation would be "Binary".
    - For the Word alphabet with letter equal to DNA and length equal to 3, the string representation would be "Word(letter=DNA, length=3)".
    - For the Codon alphabet with letter equal to RNA, the string representation would be "Codon(letter=RNA)".

*)

type model =
  | JC69
  | K80 of { kappa : float option }
  (** Type representing different evolutionary models used in bioinformatics.
      - [JC69]: Represents the JC69 model.
      - [K80 { kappa }]: Represents the K80 model with an optional kappa value. *)

val string_of_model : model -> string
(** [string_of_model model] converts an evolutionary model value to its string representation.
    The resulting string represents the type of model along with any additional parameters,
    such as the kappa value for the K80 model. *)

module Cmd : sig
  (** This module provides functions to generate command-line strings for executing external commands related to the BppSuite package, such as 'bppml' and 'bppseqgen'. *) 

  val bppml :
    alphabet:alphabet ->
    model:model ->
    input_tree_file:string ->
    input_sequence_file:string ->
    ?output_tree_file:string ->
    unit ->
    string
  (** [bppml ~alphabet ~model ~input_tree_file ~input_sequence_file ?output_tree_file ()]
      constructs a command string for executing the bppml program with the given parameters.
      It returns the formatted command string. The [alphabet] parameter specifies the input alphabet,
      [model] specifies the evolutionary model, [input_tree_file] is the path to the input tree file,
      and [input_sequence_file] is the path to the input sequence file. The optional [output_tree_file]
      parameter specifies the output tree file path, which can be omitted. *)

  val bppseqgen :
    alphabet:alphabet ->
    model:model ->
    number_of_sites:int ->
    input_tree_file:string ->
    output_sequence_file:string ->
    string
    (** [bppseqgen ~alphabet ~model ~number_of_sites ~input_tree_file ~output_sequence_file]
        constructs a command string for executing the bppseqgen program with the given parameters.
        It returns the formatted command string. The [alphabet] parameter specifies the output alphabet,
        [model] specifies the evolutionary model, [number_of_sites] is the desired number of sites in
        the generated sequence, [input_tree_file] is the path to the input tree file, and
        [output_sequence_file] is the path to the output sequence file. *)
end
