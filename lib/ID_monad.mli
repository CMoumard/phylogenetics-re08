(** A state monad to generate integer identifiers *)

type 'a t

(** [return x] creates a state monad that returns the value [x].
    The state remains unchanged.

    {[
      let my_monad = return 42
    ]}
*)
val return : 'a -> 'a t

(** [let* m f] applies the state monad [m] to obtain a value, then passes that value to the function [f].
    The resulting state monad is applied to the updated state.

    {[
      let my_monad = let* value = my_state_monad in
        return (value + 1)
    ]}
*)
val (let*) : 'a t -> ('a -> 'b t) -> 'b t

(** [let+ m f] applies the state monad [m] to obtain a value, then applies the function [f] to that value.
    The resulting value is wrapped in a new state monad with the same state.

    {[
      let my_monad = let+ value = my_state_monad in
        value + 1
    ]}
*)
val (let+) : 'a t -> ('a -> 'b) -> 'b t

(** [new_id] is a state monad that generates a new identifier and increments the state by 1.
    The generated identifier is returned as the result.

    {[
      let my_monad = new_id
    ]}
*)
val new_id : int t

(** [run m] runs the state monad [m] starting with an initial state of 0.
    It returns the value obtained from the monadic computation.

    {[
      let result = run my_state_monad
    ]}
*)
val run : 'a t -> 'a
