
type node_info = {
  seq_id : int;
}

type branch_info = {
  length : float;
}

module type AlphabetBis = sig 
  include Alphabet.S_int
  val to_char : t -> char 

  val of_char_exn : char -> t
end


module Make(A : AlphabetBis) = struct 

  (*=================
        A_WITH_GAP 
    =================*)
  module A_with_gap = struct 
    include Alphabet.Make(struct 
      let card = A.card + 1 
    end)



    let is_gap residue = (residue = A.card)

    type repr = Gap | Residue of A.t

    let to_char t =
      if is_gap t then 
        '_'
      else
        A.to_char (A.of_int_exn (t :> int)) 

    let gap = A.card

    let of_char_exn c = 
      if c = '_' || c = '-' then 
        gap
      else 
        (A.of_char_exn c :> int)

    let classify (r : t) : repr = 
      if is_gap r then 
        Gap
      else 
        Residue (A.of_int_exn (r :> int))

    let to_A_with_gap (r : A.t) : t = (r :> int)

  
    let dotv_from_A (mat : A.Matrix.t) (vect : A.Vector.t) : Vector.t = 
      let new_vec = Linear_algebra.dotv ?length:(Some card) (mat :> Linear_algebra.mat) (vect :> Linear_algebra.vec) in 
      assert (Vector.length new_vec = card);
      assert (Vector.get new_vec (card - 1) = 0.);
      new_vec
    let to_A_Vector (v : Vector.t) : A.Vector.t =
      (* let array1 = Linear_algebra.Vector.to_bigarray_array1 v in  *)
      (* let vec_A = (Bigarray.Array1.sub array1 0 (A.card-1) |> A.Vector.of_bigarray_array1) in  *)
      let vec_A = A.Vector.init (fun i -> Vector.get v (i :> int)) in
      let _ = assert (A.Vector.length vec_A = (A.card)) in 
      vec_A
  end

  (* =================
        SIMULATOR
    ================= *)
  module Make_simulator = struct 



    type tree = (node_info, node_info, branch_info) Tree.t

    (*====================*)
    (* MODULES AND IMPORT *)
    (*====================*)
  
    (* Alphabet with the gap character module *)
    

    module Sequence = struct 
      open Core
      type t = A_with_gap.t array
      let length = Array.length
      let get = Array.get
      let set = Array.set
      let init = Array.init
      let realloc seq new_size =
        let len = Array.length seq in
        let new_arr = Array.init ~f:(fun i -> if i < len then seq.(i) else A_with_gap.gap) new_size in
        assert (Array.length new_arr = new_size);
        new_arr

      let insert_gaps seq ~pos ~insert_len = 
        let final_len = Array.length seq + insert_len in 
        Array.init final_len ~f:(fun i ->
          if i <= pos then seq.(i)
          else if i <= pos + insert_len then A_with_gap.gap
          else seq.(i - insert_len)
        ) 

      let to_string seq = 
        Array.map ~f:(fun a_with_gap -> A_with_gap.to_char a_with_gap) seq
          |> Array.to_list 
          |> String.of_char_list

    end 

    (* Multiple sequence alignment module *)
    module MSA = struct 
      
      type t = {
        names : string option array ;
        tags : Newick_ast.tag list array;
        descriptions : string array ;
        sequences : Sequence.t array ;
      }

      let sequence t i = t.sequences.(i)
    
      (* Récupère la description de la séquence *)
      let description t i = t.descriptions.(i)
    
      (* Récupère le nombre de séquences *)
      let nrows a = Sequence.length a.sequences
    
      (* Récupère le nombre de colonnes *)
      let ncols a = Sequence.length  a.sequences.(0)
    
      let replace_seq t i new_seq = 
        t.sequences.(i) <- new_seq

      let init nbseqs length = {
        names = Array.make nbseqs None;
        tags = Array.make nbseqs [];
        descriptions = Array.make nbseqs "" ;
        sequences = Array.init nbseqs (fun _ -> Sequence.init length ~f:(fun _ -> A_with_gap.gap)) ;
      }

      let set t i j a = 
        assert (0 <= i && i < nrows t);
        assert (0 <= j && j < ncols t);
        assert ((A_with_gap.to_int a) < A_with_gap.card);
        t.sequences.(i).(j) <- a


      let tree_of_site { sequences ; _ } ~(tree:tree) ~site =
          Tree.map tree 
            ~branch:Core.Fn.id
            ~node:Core.Fn.id
            ~leaf:(fun { seq_id } -> 
                let seq = sequences.(seq_id) in 
                seq.(site)  
            )

      let get_trees_of_msa msa ~(tree:tree) = 
        Core.Array.init (ncols msa) ~f:(fun i -> tree_of_site msa ~tree ~site:i)

      let to_alignment {sequences; _} = 
        let seqs = Array.mapi (fun i seq -> (Printf.sprintf "seq%d" i, Sequence.to_string seq)) sequences in
        match Alignment.of_assoc_list (Array.to_list seqs) with 
        | Ok seq -> seq 
        | Error _ -> failwith "Error when create an alignment"
      
      let of_alignement_RE08 (aln : Alignment.t) = 
        let assoc_list = Alignment.to_assoc_list aln in
        let seqs_with_prefix = 
          assoc_list
        in
        let nbseqs = 
          seqs_with_prefix 
          |> Core.List.length
        in
        let msa = init nbseqs (Alignment.ncols aln) in
        (* On trie assoc_list selon l'ordre lexicographique de la première string du couple *)
        let sorted_assoc_list = 
          seqs_with_prefix 
          |> Core.List.sort ~compare:(fun (name1, _) (name2, _) -> Core.String.compare name1 name2)
        in
        (* On ajoute les séquences dans l'ordre leur apparition dans sorted_assoc_list *)
        Core.List.iteri sorted_assoc_list ~f:(fun i (_, seq) -> 
          msa.sequences.(i) <- Sequence.init (Alignment.ncols aln) ~f:(fun j -> A_with_gap.of_char_exn seq.[j])
        );
        (* On renvoie une table de hashage associant à chaque nom de séquence dans l'alignement son indice dans msa *)
        let seqs_names = 
          sorted_assoc_list 
          |> Core.List.mapi ~f:(fun i (name, _) -> (name, i))
          |> Core.String.Table.of_alist_exn
        in
        msa, seqs_names

      let pp (msa : t) = 
        Array.iteri (fun i seq -> 
          Printf.printf "seq%d: %s\n" i (Sequence.to_string seq)
        ) msa.sequences
        
        


    end



    module RM = Rate_matrix.Make(A)
    module RM_with_gap = Rate_matrix.Make(A_with_gap)


    (*=======*)
    (* TYPES *)
    (*=======*)

    type param = {
      exchangeability : A.matrix;
      stationary_distribution : A.vector;
      insertion_rate : float;
      deletion_rate : float;
      inserted_residues_proba : A.vector;
    }


    type terate = {
      rate_matrix : A_with_gap.Matrix.t; (* substitution rate matrix *)
      xit : float -> float; (* function of deletion rate *)
      gammat : float -> float; (* function of insertion rate *)
      p : float; (* probability bernoulli parameter for ancestral length distribution *)

      frq : A.vector; (* residue frequencies *)
      is_fixed_len : bool; (* if true, the ancestral length distribution is fixed to p *)
      size_ancestral_seq : int (* if is_fixed_len is false, the ancestral length distribution is geometric of parameter p *)
    }



    (*====================*)
    (* TYPES CONSTRUCTORS *)
    (*====================*)


    let terate_create rate_matrix ins_rate del_rate ?(bernoulli=0.) ?(size_ancestral_seq=0) frq_residues is_fixed_len = 
      let xit time = 
        if del_rate > 0. && ins_rate > 0. then 
          ins_rate /. (ins_rate +. del_rate) *. (1. -. exp(-. (ins_rate +. del_rate) *. time))
        else if del_rate > 0. then 0.
        else if ins_rate > 0. then 1. -. exp(-. ins_rate *. time)
        else 0.
      in
      let gammat time = 
        if del_rate > 0. && ins_rate > 0. then 
          del_rate /. (ins_rate +. del_rate) *. (1. -. exp(-. (ins_rate +. del_rate) *. time))
        else if del_rate > 0. then 1. -. exp(-. del_rate *. time)
        else if ins_rate > 0. then 0.
        else 0.
      in
      { rate_matrix; xit; gammat; p=bernoulli; frq=frq_residues; is_fixed_len; size_ancestral_seq }


    (*==========================*)
    (* RATE MATRIX CONSTRUCTION *)
    (*==========================*)


    let residues_rate_matrix {exchangeability; stationary_distribution; _} = 
      let f residue1 residue2 =
        exchangeability.A.%{residue1, residue2} *. stationary_distribution.A.%(residue2)
      in 
      RM.make f 

    
    
    let rate_matrix ({insertion_rate = insertion_rate; deletion_rate=deletion_rate ; inserted_residues_proba=inserted_residues_proba;_} as p) = 
      let residues_rm = residues_rate_matrix p in
      let f residue1 residue2 = 
        match A_with_gap.classify residue1, A_with_gap.classify residue2 with 
        | Gap, Gap -> Core.failwithf "%d %d" (residue1 :> int) (residue2 :> int) ()
        | Gap, Residue r -> inserted_residues_proba.A.%(r) *. insertion_rate
        | Residue _, Gap ->  deletion_rate
        | Residue r1, Residue r2 -> 
          assert (
            residues_rm.A.%{r1, r2} -. 
            (if A.equal r1 r2 then 
              deletion_rate 
            else 
              0.) > 0.);
          residues_rm.A.%{r1, r2} -. 
          (if A.equal r1 r2 then 
            deletion_rate 
          else 
            0.)
        in
      RM_with_gap.make f 


    (*============*)
    (* SIMULATION *)
    (*============*)

    

    let add_l_residues rng erate l msa sequence_id start =
      assert ((start + l) <= (Sequence.length msa.MSA.sequences.(sequence_id))); 
      assert (start >= 0);
      let rec loop i =
        if i >= start + l then ()
        else
          let residue = A.sample rng erate.frq in
          assert ((A_with_gap.to_A_with_gap residue) <> A_with_gap.gap);
          MSA.set msa sequence_id i (A_with_gap.to_A_with_gap residue);
          loop (i + 1)
        in
        loop start
    (** [add_l_residues rng erate l msa sequence_id start] adds [l] residues to the sequence [sequence_id] of [msa] starting at position [start] *)
    

    
    let generate_root_sequence rng nb_taxons root terate =
      let bernouillip = terate.p in
      (*TODO: initialiser les noms des séquences (ajouter le champ)*)
      let length = 
        if terate.is_fixed_len then 
          (* Float.to_int (bernouillip /. (1. -. bernouillip)) *)
          terate.size_ancestral_seq
        else 
          (* determine the value of the variable L using a geometric distribution *)
          Gsl.Randist.geometric rng ~p:bernouillip
      in
      (* let _ = Format.printf "Sequence ancestral length : %d\n%!" length in *)
      let msa = MSA.init nb_taxons length in
      add_l_residues rng terate length msa root.seq_id 0;
      msa
    (** [generate_root_sequence rng nb_taxons root terate] generates the root sequence of the tree 
        
        [rng] is the random number generator
        [nb_taxons] is the number of taxa
        [root] is the root node
        [terate] is the transition rate matrix
        
        The length of the root sequence is determined by the bernoulli parameter of the ancestral length distribution. 
        If the parameter is fixed, the length is determined by terate.size_ancestral_seq.
        Otherwise, the length is determined by a geometric distribution with parameter p.
    *)


    let subst_or_deletion ?verbose ?nb_deletions ?nb_substitutions rng col p d q_eps msa =
      
      assert (Array.length msa.MSA.sequences > p && Array.length msa.MSA.sequences.(p) > col);
      if A_with_gap.is_gap msa.MSA.sequences.(p).(col) then
        msa.MSA.sequences.(d).(col) <- A_with_gap.gap
      else
        (
        assert (col >= 0 && col < Array.length msa.MSA.sequences.(p));
        assert (p >= 0 && p < Array.length msa.MSA.sequences);
        assert (msa.MSA.sequences.(p).(col) <> A_with_gap.gap);
        let residue = msa.MSA.sequences.(p).(col) in
      
        let vector_for_residue =
          try
          A_with_gap.Matrix.row q_eps (A_with_gap.to_int residue)
          with _ -> let x,y = A_with_gap.Matrix.dim q_eps in
          Core.failwithf "Matrix dim : [%d, %d]\nresidue : [%d]" x y (A_with_gap.to_int residue) ()
        in
        let residue_to_substitute_or_delete = A_with_gap.sample rng vector_for_residue in
        match verbose with
        | Some true -> 
            (match A_with_gap.classify residue_to_substitute_or_delete with 
            | Gap -> 
              Format.printf "Deletion at column %d of sequence %d\n%!" col d;
              (match nb_deletions with 
              | Some nb_deletions -> nb_deletions := !nb_deletions + 1
              | None -> ())
            | Residue _ -> 
              Format.printf "Substitution at column %d of sequence %d\n%!" col d;
              (match nb_substitutions with 
              | Some nb_substitutions -> nb_substitutions := !nb_substitutions + 1
              | None -> ()))
        | _ -> ();
        (* assert (d <> 0); *)
        msa.MSA.sequences.(d).(col) <- residue_to_substitute_or_delete
        )
    (** [subst_or_deletion rng col p d terate msa] performs a substitution or a deletion at column [col] of the sequence [d] of [msa] using the sequence [p] of [msa] as the parent sequence.
        
        [rng] is the random number generator
        [col] is the column
        [p] is the parent sequence
        [d] is the daughter sequence
        [terate] is the transition rate matrix
        [msa] is the multiple sequence alignment

      If the residue at column [col] of the parent sequence is a gap, then the residue at column [col] of the daughter sequence is set to a gap.
      Otherwise, a substitution is performed using the transition rate matrix.
    *)


    let insert ?verbose ?nb_insertions rng col p d terate msa time =
      if col >= 0 && msa.MSA.sequences.(p).(col) = A_with_gap.gap then 
        (* if the parent residue is a gap, then the daughter residue is a gap.  
          The condition col >= 0 is necessary to avoid inserting a gap at the beginning of the sequence. *)
        ()
      else
        let find_l r q =
          let l = ref 0 in
          let pdf = ref (1.0 -. q) in
          let fac = ref !pdf in
        
          let x = Gsl.Rng.uniform r in
          while !pdf < x do
            l := !l + 1;
            fac := !fac *. q;
            pdf := !pdf +. !fac;
          done;
        
          !l
        in        
        let q = terate.xit time in 
        let insert_len = find_l rng q in
        (* let _ = Format.printf "Insertion length : %d\n%!" insert_len in *)
        (* On observe un nombre d'insertions élevé ici si la probabilité d'insertion
        est très basse. Faible probabilité d'insertion => faible xit => le paramètre p de Bernouilli de 
        la loi géométrique est faible => La probabilité de réussir est faible, donc il faut beaucoup d'insertions avant
        d'en réussir une. *)
        (match verbose with
        | Some true -> 
          (* Format.printf "Insertion of length %d at column %d of sequence %d\n%!" insert_len col d; *)
          (match nb_insertions with 
          | Some nb_insertions -> nb_insertions := !nb_insertions + insert_len
          | None -> ())
        | _ -> ());
        if insert_len <> 0 then 
          let prec_length = Sequence.length msa.MSA.sequences.(0) in
          let () = Core.Array.iteri ~f:(fun i seq -> 
            let new_seq = Sequence.insert_gaps seq ~pos:col ~insert_len in 
            msa.MSA.sequences.(i) <- new_seq
            ) msa.MSA.sequences in 
          let _ = assert (insert_len > 0) in
          let _ = assert (Array.length msa.MSA.sequences.(0) = (prec_length + insert_len)) in
          (* let _ = Format.printf "New length : %d\n%!" (Array.length msa.MSA.sequences.(0)) in
          let _ = Format.printf "prec_length : %d\n%!" prec_length in
          let _ = Format.printf "insert_len : %d\n%!" insert_len in *)
          (* let _ = exit 0 in *)
          let _ = add_l_residues rng terate insert_len msa d (col + 1) in 
          ()
    (** [insert rng col p d terate msa time] performs an insertion at column [col] of the sequence [d] of [msa] using the sequence [p] of [msa] as the parent sequence.
        
        [rng] is the random number generator
        [col] is the column
        [p] is the parent sequence
        [d] is the daughter sequence
        [terate] is the transition rate matrix
        [msa] is the multiple sequence alignment
        [time] is the time of the insertion
        
        The length of the insertion is determined by a geometric distribution with parameter q = xit(time).
        The insertion is performed by reallocating the sequences of the multiple sequence alignment.
        The residues of the insertion are sampled from the equilibrium distribution of the transition rate matrix.
    *)


    let evolve_ascendant_to_descendant ?verbose ?nb_insertions ?nb_deletions ?nb_substitutions rng p d time terate msa =
      let q_eps = A_with_gap.Matrix.(expm (scal_mul time terate.rate_matrix)) in 
      let rec loop_over_all_cols u f = 
        if u >= (Sequence.length msa.MSA.sequences.(p)) then ()
        else (
          f u ; loop_over_all_cols (u + 1) f)
      in 

      (*
        SUBSTITUTION OR DELETION
        Each residue is either substituted or deleted.
        Gaps are propagated as gaps.
      *)
      let _ = loop_over_all_cols 0 (fun u -> subst_or_deletion ?verbose ?nb_deletions ?nb_substitutions rng u p d q_eps msa) in 

      (*
        INSERTIONS
        Each link puts an insertion, possibly of length zero.
        A link is defined after a residue, not after a gap.
        Assign u=-1 for the immortal link insertion.
      *)
      
      let _ = insert ?verbose ?nb_insertions rng (-1) p d terate msa time in 
      
      let _ = loop_over_all_cols 0 (fun u -> insert ?verbose ?nb_insertions rng u p d terate msa time) in 

      (*
        Check that all sequences have the same length.
      *)
      let _ = 
        let rec loop_over_all_seqs i = 
          if i >= Array.length msa.MSA.sequences then ()
          else 
            (
            assert (Sequence.length msa.MSA.sequences.(i) = Sequence.length msa.MSA.sequences.(0));
            loop_over_all_seqs (i + 1))
        in
        loop_over_all_seqs 0
      in
      ()

    (** [evolve_ascendant_to_descendant rng p d time terate msa] evolves the sequence [p] of [msa] to the sequence [d] of [msa] using the transition rate matrix [terate] and the time [time].
        
        [rng] is the random number generator
        [p] is the parent sequence
        [d] is the daughter sequence
        [time] is the time of the insertion
        [terate] is the transition rate matrix
        [msa] is the multiple sequence alignment
        
        The evolution is performed by performing substitutions, deletions and insertions.
    *)

    let evolve_root ?nb_insertions ?verbose ?nb_deletions ?nb_substitutions rng tree terate msa = 
      Tree.preorder_traversal_with_parent tree 
        ~f:(fun child_data parent_data branch_info ->
            evolve_ascendant_to_descendant ?verbose ?nb_insertions ?nb_deletions ?nb_substitutions rng parent_data.seq_id child_data.seq_id branch_info.length terate msa
        )
    (** [evolve_root rng tree terate msa] evolves the root sequence of [tree] using the transition rate matrix [terate] and the multiple sequence alignment [msa].
        
        [rng] is the random number generator
        [tree] is the tree
        [terate] is the transition rate matrix
        [msa] is the multiple sequence alignment
        
        The evolution is performed by performing substitutions, deletions and insertions.
        At each node, the parent sequence is evolved to the daughter sequence with the branch length as the time.
    *)
    


    (*===============*)
    (* MAIN FUNCTION *)
    (*===============*)
    
    let generate_alignment rng tree_extended ?verbose terate =
      let nb_insertions = ref 0 in
      let nb_deletions = ref 0 in
      let nb_substitutions = ref 0 in
      let nb_taxons = tree_extended.Tree.node_count in
      let msa = generate_root_sequence rng nb_taxons (Tree.data (tree_extended.tree)) terate in
      let _ = evolve_root ?verbose ~nb_insertions:nb_insertions ~nb_deletions:nb_deletions ~nb_substitutions:nb_substitutions rng tree_extended.tree terate msa in  
      let _ = Format.printf "MSA generated. Length : %d\n%!" (Array.length msa.MSA.sequences.(0)) in
      let _ = assert (Array.length msa.MSA.sequences.(0) = Sequence.length msa.MSA.sequences.(0)) in
      (* let _ = assert (Array.length msa.MSA.sequences.(0) = 100) in *)
      let _ = Format.printf "\n%!" in
      msa, !nb_insertions, !nb_deletions, !nb_substitutions
    (** [generate_alignment rng tree_extended terate] generates a multiple sequence alignment from a tree and a transition rate matrix.
        
        [rng] is the random number generator
        [tree_extended] is the tree
        [terate] is the transition rate matrix
        
        The multiple sequence alignment is generated by first generating the root sequence and then evolving the root sequence to the other sequences.
    *)





    let p = 
      let exchangeability = A.Matrix.init (fun i j -> if i = j then -. (float A.card) else 1.) in
      let stationary_distribution = A.Vector.init (fun _ -> 1./. float A.card) in 
      let insertion_rate = 0.1 in 
      let deletion_rate = 0. in 
      let inserted_residues_proba = A.Vector.init (fun _ -> 1./. float A.card) in 
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba }
  
  end

  (* =============
       INFERENCE
    ==============*)
  module Inference = struct 

    module Binary_tree : sig 
      val postfix_traversal :
      ('a, 'b, 'c) Tree.t ->
      leaf:('b -> 'd) ->
      node:('d -> 'd -> 'c -> 'c -> ('a, 'b, 'c) Tree.t -> ('a, 'b, 'c) Tree.t -> 'd) ->
      'd
    end = struct 
      include Tree 

      let postfix_traversal binary_tree ~leaf ~node =
        let rec aux binary_tree =
          match binary_tree with
          | Leaf residue -> leaf residue  
          | Node n ->
            let branches = List1.couple n.branches in
            match branches with
            | None -> failwith "Error: binary tree with an odd number of branches"
            | Some (Branch left , Branch right) ->
              let freq1 = aux left.tip in 
              let freq2 = aux right.tip in
              node freq1 freq2 left.data right.data left.tip right.tip
        in
        aux binary_tree

    end 
    
    type param = {
      exchangeability : A.matrix;
      stationary_distribution : A.vector;
      insertion_rate : float;
      deletion_rate : float;
      inserted_residues_proba : A.vector;
      bernouilli : float;
    }

    type te = {
      dim : int;
      p : float;
      t : float;
      xit : float;
      gammat : float;
      frq : A.vector;
    }



    module RM = Rate_matrix.Make(A)
    module RM_with_gap = Rate_matrix.Make(A_with_gap)

    let residues_rate_matrix {exchangeability; stationary_distribution; _} = 
      let f residue1 residue2 =
        exchangeability.A.%{residue1, residue2} *. stationary_distribution.A.%(residue2)
      in 
      RM.make f 
    let rate_matrix ({insertion_rate = insertion_rate; deletion_rate=deletion_rate ; inserted_residues_proba=inserted_residues_proba;_} as p) = 
      let residues_rm = residues_rate_matrix p in
      let f residue1 residue2 = 
        match A_with_gap.classify residue1, A_with_gap.classify residue2 with 
        | Gap, Gap -> Core.failwithf "%d %d" (residue1 :> int) (residue2 :> int) ()
        | Gap, Residue r -> inserted_residues_proba.A.%(r) *. insertion_rate
        | Residue _, Gap ->  deletion_rate
        | Residue r1, Residue r2 -> 
          assert (
            residues_rm.A.%{r1, r2} -. 
            (if A.equal r1 r2 then 
              deletion_rate 
            else 
              0.) > 0.);
          residues_rm.A.%{r1, r2} -. 
          (if A.equal r1 r2 then 
            deletion_rate 
          else 
            0.)
        in
      RM_with_gap.make f 

    let proba_matrix ({insertion_rate = insertion_rate; deletion_rate=deletion_rate ;stationary_distribution = stationary_distribution; _}) time = 
      let xi = deletion_rate /. (insertion_rate +. deletion_rate) *. (1. -. exp(-. (insertion_rate +. deletion_rate) *. time)) in 
      let gamma = insertion_rate /. (insertion_rate +. deletion_rate) *. (1. -. exp(-. (insertion_rate +. deletion_rate) *. time)) in
      assert (gamma > 0.);
      assert (xi > 0.);
      let f residue1 residue2 = 
        match A_with_gap.classify residue1, A_with_gap.classify residue2 with 
        | Gap, Gap -> 1. -. gamma 
        | Gap, Residue r -> xi *. stationary_distribution.A.%(r)
        | Residue _, Gap -> xi 
        | Residue _, Residue _ -> 0. 
      in
      RM_with_gap.make f 


  

    let f84_rate_matrix_nucleotide_maker ({insertion_rate = lambda; deletion_rate=mu ;_} as p) ?(ttr = 2.) ?alpha ?beta time  =

      Format.printf "p.stationary_distribution : %a\n%!" A.Vector.pp p.stationary_distribution;

      let frqr = ref (p.stationary_distribution.A.%(A.of_int_exn 0) +. p.stationary_distribution.A.%(A.of_int_exn 2)) in
      let frqy = ref (p.stationary_distribution.A.%(A.of_int_exn 1) +. p.stationary_distribution.A.%(A.of_int_exn 3)) in
      let frqpr = ref (p.stationary_distribution.A.%(A.of_int_exn 0) *. p.stationary_distribution.A.%(A.of_int_exn 2)) in
      let frqpy = ref (p.stationary_distribution.A.%(A.of_int_exn 1) *. p.stationary_distribution.A.%(A.of_int_exn 3)) in


      let aa = ttr *. !frqr *. !frqy -. !frqpr -. !frqpy in
      let bb = !frqpr /. !frqr +. !frqpy /. !frqy in 
      let alpha = 
        match alpha with
        | Some alpha -> alpha
        | None -> aa /. (aa +. bb) in
      let beta =
        match beta with
        | Some beta -> beta
        | None ->
        1. -. alpha in

      let frqar = ref 0. in 
      let frqgr = ref 0. in 
      let frqcy = ref 0. in
      let frqty = ref 0. in

      let mat = A.Matrix.init (fun _ _ -> 0.) in

      if !frqr > 0. then 
        (frqar := p.stationary_distribution.A.%(A.of_int_exn 0) /. !frqr;
        frqgr := p.stationary_distribution.A.%(A.of_int_exn 2) /. !frqr;);
      if !frqy > 0. then 
        ( frqar := p.stationary_distribution.A.%(A.of_int_exn 1) /. !frqy;
        frqgr := p.stationary_distribution.A.%(A.of_int_exn 3) /. !frqy;);
      for i = 0 to A.card - 1 do
        
        let term1 = ref 0. in 
        let term2 = ref 0. in 
        let term3 = ref 0. in 
        let term4 = ref 0. in 
        for j = 0 to A.card - 1 do
          if mu > 0. && lambda > 0. then
            term1 := lambda /. (lambda +. mu) *. (p.stationary_distribution.A.%(A.of_int_exn j))
          else if mu > 0. then term1 := 0.
          else if lambda > 0. then term1 := p.stationary_distribution.A.%(A.of_int_exn j)
          else term1 := p.stationary_distribution.A.%(A.of_int_exn j);
          
          if i = 0 && j = 0 then 
            term2 := !frqar -. p.stationary_distribution.A.%(A.of_int_exn 0)
          else if i = 0 && j = 2 then
            term2 := !frqgr -. p.stationary_distribution.A.%(A.of_int_exn 2)
          else if i = 2 && j = 0 then
            term2 := !frqar -. p.stationary_distribution.A.%(A.of_int_exn 0)
          else if i = 2 && j = 2 then
            term2 := !frqgr -. p.stationary_distribution.A.%(A.of_int_exn 2)
          else if i = 1 && j = 1 then
            term2 := !frqcy -. p.stationary_distribution.A.%(A.of_int_exn 1)
          else if i = 1 && j = 3 then
            term2 := !frqty -. p.stationary_distribution.A.%(A.of_int_exn 3)
          else if i = 3 && j = 1 then
            term2 := !frqcy -. p.stationary_distribution.A.%(A.of_int_exn 1)
          else if i = 3 && j = 3 then
            term2 := !frqty -. p.stationary_distribution.A.%(A.of_int_exn 3)
          else term2 := -.p.stationary_distribution.A.%(A.of_int_exn j);

          term2 := !term2 *. exp(-. (beta +. mu) *. time);

          if i = 0 && j = 0 then 
            term3 := 1. -. !frqar
          else if i = 0 && j = 2 then
            term3 := -. !frqgr
          else if i = 2 && j = 0 then
            term3 := -. !frqar
          else if i = 2 && j = 2 then
            term3 := 1. -. !frqgr
          else if i = 1 && j = 1 then
            term3 := 1. -. !frqcy
          else if i = 1 && j = 3 then
            term3 := -. !frqty
          else if i = 3 && j = 1 then
            term3 := -. !frqcy
          else if i = 3 && j = 3 then
            term3 := 1. -. !frqty
          else if i = j then term3 := 1.
          else term3 := 0.;

          term3 := !term3 *. exp(-. (alpha +. beta +. mu) *. time);

          if mu > 0. && lambda > 0. then 
            term4 := mu /. (lambda +. mu) *. (p.stationary_distribution.A.%(A.of_int_exn j)) *. exp(-. (lambda +. mu) *. time)
          else if mu > 0. then term4 := p.stationary_distribution.A.%(A.of_int_exn j) *. exp(-. mu *. time)
          else if lambda > 0. then term4 := 0.
          else term4 := 0.;


          (* Format.printf "i %d j %d term1 %f term2 %f term3 %f term4 %f\n" i j !term1 !term2 !term3 !term4; *)
          
          
          mat.A.%{A.of_int_exn i, A.of_int_exn j} <- !term1 +. !term2 +. !term3 +. !term4;

          if mat.A.%{A.of_int_exn i, A.of_int_exn j} < 0. then 
            if mat.A.%{A.of_int_exn i, A.of_int_exn j} > -.1e-10 then 
              mat.A.%{A.of_int_exn i, A.of_int_exn j} <- 0.
            else 
              Core.failwithf "Error: negative value in the rate matrix: %f" mat.A.%{A.of_int_exn i, A.of_int_exn j} ()
        done;
        if mat.A.%{A.of_int_exn i, A.of_int_exn i} > 1. then 
          Core.failwithf "Error: diagonal value in the rate matrix > 1: %f" mat.A.%{A.of_int_exn i, A.of_int_exn i} ()
      done;
      mat 


          
    let qre ({insertion_rate = insertion_rate; deletion_rate=deletion_rate ; _} as param) time = 
      let rate_matrix = rate_matrix param in 
      let q_eps = A_with_gap.Matrix.(expm (scal_mul time rate_matrix)) in 
      let xi = 
        match insertion_rate, deletion_rate with
        | 0., 0. -> 0.
        | _, _ -> 
        deletion_rate /. (insertion_rate +. deletion_rate) *. (1. -. exp(-. (insertion_rate +. deletion_rate) *. time)) in 
      A_with_gap.Matrix.(scal_mul (1. -. xi) q_eps)



    let qsim param time = 
      let open Simulator.Make(A_with_gap) (struct type t = float let length t = t end) in 
      let rng = Gsl.Rng.make Gsl.Rng.BOROSH13 in 
      let rate_matrix = rate_matrix param in 
      let branch_length = time in 
      let mat = A.Matrix.init (fun _ _ -> 0.) in 
      let _B_ = 990000 in 
      let exception Found_gap in 
      for i = 0 to A.card - 1 do 
        for _ = 1 to _B_ do 
          let rec loop () = 
            try 
              let final_res = branch_gillespie_direct rng ~start_state:(A_with_gap.of_int_exn i) ~rate_matrix ~branch_length 
                    ~init:(A.of_int_exn i) ~f:(fun _ next_state _ -> 
                        match A_with_gap.classify next_state with 
                        | Residue res2 ->  res2
                        | Gap -> raise Found_gap
                    ) in 
                A.Matrix.set mat i (A.to_int final_res) ((A.Matrix.get mat i (A.to_int final_res)) +. 1.)
            with 
              | Found_gap -> loop () 
          in loop ()
        done;
      done;
    A.Matrix.scal_mul (1. /. float _B_) mat  
    let p5 = 
      let stationary_distribution = 
        A.Vector.of_array_exn [| 0.1; 0.2 ; 0.3 ; 0.4 |]
        |> A.Vector.normalize in
      let exchangeability = A.Matrix.init (fun i j -> if i = j then -. (float A.card) else (float (A.to_int i) +. float (A.to_int j))/.10.) in
      let insertion_rate = 0. in 
      let deletion_rate = 2. in 
      let inserted_residues_proba = stationary_distribution in
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba ; bernouilli = 0. }
    
    
    let p4 = 
      let stationary_distribution = 
        A.Vector.of_array_exn [| 0.25 ; 0.25 ; 0.25 ; 0.25 |]
        |> A.Vector.normalize in
      let exchangeability = A.Matrix.init (fun i j -> if i = j then -. (float A.card) else (float (A.to_int i) +. float (A.to_int j))/.10.) in
      let insertion_rate = 0.3 in 
      let deletion_rate = 0.2 in 
      let inserted_residues_proba = stationary_distribution in
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba ; bernouilli = 0. }
    
  
    let p3 = 
      let exchangeability = A.Matrix.init (fun i j -> if i = j then -. (float A.card) else (float (A.to_int i) +. float (A.to_int j))/.10.) in
      let stationary_distribution = A.Vector.init (fun i -> (1. +. float (A.card - (A.to_int i)))) in 
      let insertion_rate = 0. in 
      let deletion_rate = 5. in 
      let inserted_residues_proba = A.Vector.normalize (A.Vector.init (fun _ -> 1.)) in 
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba; bernouilli = 0. }

    let p2 = 
      let exchangeability = A.Matrix.init (fun i j -> if i = j then -. (float A.card) else 0.1) in
      let stationary_distribution = A.Vector.init (fun i -> (1. +. float (A.card - (A.to_int i)))) |> A.Vector.normalize in 
      let insertion_rate = 10. in 
      let deletion_rate = 5. in 
      let inserted_residues_proba = A.Vector.normalize (A.Vector.init (fun i -> (exp (20. *. (1. +. float (A.to_int i)))))) in 
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba; bernouilli = 0. }
    
    let p1 = 
      let exchangeability = A.Matrix.init (fun i j -> if i = j then -. (float A.card) else 1.) in
      let stationary_distribution = A.Vector.init (fun _ -> 1./. float A.card) in 
      let insertion_rate = 0.5 in 
      let deletion_rate = 0.1 in 
      let inserted_residues_proba = A.Vector.init (fun _ -> 1./. float A.card) in 
      {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba; bernouilli = 0. }
    
    
    let likelihood_rec ?f84_matrix param (tree : ('n, A_with_gap.t, branch_info) Tree.t) = 

      let xit time { deletion_rate=mu ; insertion_rate=lambda; _ } = 
        if mu = 0. && lambda = 0. then 0.
        else lambda /. (lambda +. mu) *. (1. -. exp(-. (lambda +. mu) *. time))
      in

      let gammat time { deletion_rate=mu ; insertion_rate=lambda; _ } = 
        if mu = 0. && lambda = 0. then 0.
        else
          mu /. (lambda +. mu) *. (1. -. exp(-. (lambda +. mu) *. time))
      in

      let {stationary_distribution; bernouilli; _} = param in

      let rec node_likelihood_vector ~param (node: ('n, A_with_gap.t, branch_info) Tree.t) =
        match node with 
        | Leaf residue -> 

          let obs_encoding = A_with_gap.Vector.init (fun r -> if A_with_gap.equal residue r && not (A_with_gap.is_gap residue) then 1. else 0.) in
          (* Format.printf "obs_encoding: \n"; 

          A_with_gap.Vector.pp Format.std_formatter obs_encoding; *)
          let is_gap = A_with_gap.is_gap residue in
          obs_encoding, is_gap
        | Node { branches ; _ } -> 
          
          let children = List1.map branches ~f:(fun (Tree.Branch b) -> 
            let child = b.tip in
            node_likelihood_vector ~param child, 
            b.data.length
          ) in

          let residues_prob_vector = List1.fold children ~init:(A.Vector.init (Core.Fn.const 1.)) 
          ~f:(fun acc ((child_likelihood, only_leafs_gaps), time) -> 
            (* let residues_rate_matrix = residues_rate_matrix param in *)
            let rate_matrix = (Obj.magic (rate_matrix param) : A_with_gap.matrix) in 
            (*(e^{t*R_\epsilon})_{1:k,1:k} = e^{-\mu * t} * e^{t * R}*)
            (* let proba_matrix = A.Matrix.(scal_mul (exp (-. time *. mu)) (expm (scal_mul time residues_rate_matrix))) in *)
            let proba_matrix_without_gap = 
              match f84_matrix with
              | None -> 
                let proba_matrix = A_with_gap.Matrix.(expm (scal_mul time rate_matrix)) in 
                            (* eq. 23 *)
                let proba_matrix = A_with_gap.Matrix.(scal_mul (1. -. (xit time param)) proba_matrix) in 

                A.Matrix.init (fun i j -> A_with_gap.Matrix.get proba_matrix (A_with_gap.of_int_exn (i :> int)) (A_with_gap.of_int_exn (j :> int)))
              | Some f84_matrix -> 
                A.Matrix.(scal_mul (1. -. (xit time param)) (f84_matrix time)) 
            in            
            let child_likelihood_without_gap = A_with_gap.to_A_Vector child_likelihood in 
            let residues_prob_vector = A.(dotv proba_matrix_without_gap child_likelihood_without_gap) in 
            let residues_prob_vector = if only_leafs_gaps 
              then A.Vector.map residues_prob_vector ~f:(fun x -> x +. (1. -. (xit time param)) *. gammat time param)
              else residues_prob_vector
            in
            A.Vector.mul acc residues_prob_vector)
          in

          let gap_prob, only_gaps_below = 
            match List1.filter children ~f:(fun ((_, only_leafs_gaps), _) -> Bool.not only_leafs_gaps) with 
            | [ ((child_likelihood, _), time) ] -> 
              
              let child_likelihood_without_gap = A_with_gap.to_A_Vector child_likelihood in
              let proba_insertion = 
                (* eq. 24 *)
                A.Vector.init (fun j -> (xit time param) *. stationary_distribution.A.%(j)) in 
              let insertion = A.Vector.(dot proba_insertion child_likelihood_without_gap) in 
              let child_gap = A_with_gap.Vector.get child_likelihood A_with_gap.gap in 
              insertion +. child_gap, false
            | [] -> 0., true 
            | _ ->  0., false
          in 
          (* assemble residue vector with gap prob *)
          A_with_gap.Vector.init (fun i -> 
            if A_with_gap.equal i A_with_gap.gap 
            then gap_prob 
            else A.Vector.get residues_prob_vector (A.of_int_exn i)
          ), only_gaps_below

        in

        let (root_likelihood_vector, _) = node_likelihood_vector ~param tree in 
        (* let _ = assert (root_likelihood_vector.A_with_gap.%(A_with_gap.gap) <= 1.) in  *)
        root_likelihood_vector.A_with_gap.%(A_with_gap.gap) +. 
        bernouilli *. (A.Vector.dot stationary_distribution (A_with_gap.to_A_Vector root_likelihood_vector))

              
    
    
    
    let log_likelihood_alignment ?f84_matrix ({insertion_rate=ins_rate; deletion_rate=del_rate; bernouilli = p; _} as param) trees = 
      (* eq. 29 p. 9 *)
      
      let sites_likelihood = Core.Array.fold trees ~init:1. ~f:(fun acc tree -> (*Format.print_float acc;Format.print_string " ";*) let lk = likelihood_rec ?f84_matrix param tree in let _ = assert (lk >0.) in acc +. log lk) in 
      (* let _ = Format.printf "sites_likelihood: %f\n" sites_likelihood in *)
      let xit time = 
        if del_rate > 0. && ins_rate > 0. then 
          ins_rate /. (ins_rate +. del_rate) *. (1. -. exp(-. (ins_rate +. del_rate) *. time))
        else if del_rate > 0. then 0.
        else if ins_rate > 0. then 1. -. exp(-. ins_rate *. time)
        else 0.
      in
      let _ = Array.length trees > 0 || Core.failwithf "Error: empty array of trees : alignment could be empty" () in
      let p_star = Tree.prefix_traversal trees.(0) ~init:1. ~node:(Core.Fn.const) ~leaf:(Core.Fn.const) 
            ~branch:(fun acc { length } -> acc *. (1. -. (xit length))) in
      let _ = assert ((1. -. p) *. p_star > 0.) in
      let log_p_star = log ((1. -. p) *. p_star) in 
      let gap_site = Tree.map trees.(0) ~node:(Core.Fn.id) ~leaf:(fun _ -> A_with_gap.gap) 
            ~branch:(Core.Fn.id) in 
      (* let _ = Format.printf "gap_site : \n\n\n\n\n\n" in  *)
      let gap_site_likelihood = likelihood_rec ?f84_matrix param gap_site in
      (* let _ = Format.printf "Insertion_rate et deletion_rate : %f et %f" ins_rate del_rate in  *)
      (* let _ = Format.printf "(p_root_star /. (1. -. gap_site_likelihood))  = %f\n%!" (p_root_star /. (1. -. gap_site_likelihood)) in *)
      (* let _ = Format.printf "sites_likelihood = %f\n%!" sites_likelihood in *)
      (* let _ = Format.printf "gap_site_likelihood = %f\n%!" gap_site_likelihood in *)
      (* let _ = assert ((1. -. gap_site_likelihood) > 0.) in  *)
      (* let _ = if gap_site_likelihood >= 1. then 
        (Format.printf "stationary_distribution = \n\n\n";
        A.Vector.pp Format.std_formatter param.stationary_distribution;
        Format.printf "rate_matrix = \n!";
        A_with_gap.Matrix.pp Format.std_formatter (((rate_matrix param)));
        Format.printf "expm(rate_matrix) = \n!";
        A_with_gap.Matrix.pp Format.std_formatter (A_with_gap.Matrix.(expm (scal_mul 1. (rate_matrix param))));
        Format.printf "P(i|j,1) (il ne faut pas considérer la dernière ligne/colonne) = \n";
        A_with_gap.Matrix.pp Format.std_formatter (A_with_gap.Matrix.(scal_mul (1. -. (xit 1.)) (expm (scal_mul 1. (rate_matrix param)))));
        Format.printf "P(j|-,1) = \n";
        A.Vector.pp Format.std_formatter (A.Vector.(scal_mul ((xit 1.)) param.stationary_distribution));
        ) 
      else () in *)
      let _ = assert (gap_site_likelihood < 1.) in 
      
      (* let lk = (sites_likelihood +. p) in *)
      (* let gap_site_likelihood = if gap_site_likelihood >= 1. then 0.99 else gap_site_likelihood in  *)
      log_p_star -. log (1. -. gap_site_likelihood) +. sites_likelihood
  end

end


let% expect_test "test (e^{t*R_\epsilon})_{1:k,1:k} = e^{-\mu * t} * e^{t * R}" = 
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in
  let t = 0.1 in 
  let {deletion_rate=mu ; _} = p3 in
  let rate_matrix = rate_matrix p3 in 
  let _R_ = residues_rate_matrix p3 in
  let q_eps_x_exp_mu_t = Nucleotide.Matrix.(scal_mul (exp (-. t *. mu)) (expm (scal_mul t _R_))) in
  Nucleotide.Matrix.pp Format.std_formatter q_eps_x_exp_mu_t;
  Format.print_newline ();
  Format.print_newline ();
  let q_exp = A_with_gap.Matrix.(expm(scal_mul t rate_matrix)) in
  A_with_gap.Matrix.pp Format.std_formatter q_exp;
  [%expect{|
     0.521234 0.0243087  0.031842 0.0291455
    0.0303858  0.493051 0.0454525 0.0376411
      0.05307 0.0606033  0.447877 0.0449806
    0.0728638 0.0752822 0.0674709  0.390914

     0.521234 0.0243087  0.031842 0.0291455 0.393469
    0.0303858  0.493051 0.0454525 0.0376411 0.393469
      0.05307 0.0606033  0.447877 0.0449806 0.393469
    0.0728638 0.0752822 0.0674709  0.390914 0.393469
            0         0         0         0        1 |}]

let% expect_test "test P(i|j,t)" =
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in
  let qsim = qsim p2 0.1 in 
  let qre = qre p2 0.1 in 
  Nucleotide.Matrix.pp Format.std_formatter qsim;
  Format.print_newline ();
  Format.print_newline ();
  A_with_gap.Matrix.pp Format.std_formatter qre;
  print_newline ();
  [%expect{|
     0.9934  0.0029 0.00213 0.00157
    0.00345 0.99316  0.0021 0.00129
    0.00345 0.00269 0.99239 0.00147
    0.00326 0.00273 0.00206 0.99195

       0.446712 0.00137471  0.00103104 0.100028 0.191898
     0.00171839   0.446368  0.00103104 0.100028 0.191898
     0.00171839 0.00137471    0.446024 0.100028 0.191898
     0.00171839 0.00137471  0.00103104 0.545021 0.191898
    0.000709575 0.00056766 0.000425746 0.382093 0.357247 |}]

  let% expect_test "test2 P(i|j,t)" =
    let module M = Make(Nucleotide) in 
    let open M in
    let open Inference in
    let qsim = qsim p4 0.3 in 
    let qre = qre p4 0.3 in 
    Nucleotide.Matrix.pp Format.std_formatter qsim;
    Format.print_newline ();
    Format.print_newline ();
    (* A_with_gap.Matrix.pp Format.std_formatter qre; *)
    let a_matrix = Nucleotide.Matrix.init (fun i j -> A_with_gap.Matrix.get qre (A_with_gap.of_int_exn (i :> int)) (A_with_gap.of_int_exn (j :> int))) in
    let qsim_moins_qre = Nucleotide.Matrix.init (fun i j -> Nucleotide.Matrix.get qsim (i :> int) (j :> int) /. Nucleotide.Matrix.get a_matrix (i :> int) (j :> int)) in
    Nucleotide.Matrix.pp Format.std_formatter qsim_moins_qre;
    let _ = Nucleotide.Matrix.pp Format.std_formatter a_matrix in
    [%expect{|
      0.94218 0.00601 0.01832 0.03349
      0.00296 0.92603 0.02587 0.04514
       0.0055 0.01776 0.92103 0.05571
      0.00868 0.02308 0.04143 0.92681

       11.2115 0.51312 1.00306 1.32623
      0.505436 10.4891  1.3645 1.72518
      0.903408 1.40511 9.85202  2.0594
       1.37494 1.76416 2.04202 9.30707 0.0840369 0.0117126 0.0182642  0.025252
                                      0.00585632 0.0882846 0.0189594 0.0261654
                                      0.00608805 0.0126396 0.0934865 0.0270516
                                        0.006313 0.0130827 0.0202887 0.0995813 |}]

let% expect_test "test3 P(i|j,t)" =
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in
  (* let qsim = qsim p5 0.3 in  *)
  (* let qre = f84_rate_matrix_nucleotide_maker p4 0.3 in *)
  let qsim = qsim p5 0.3 in 
  let qre = qre p5 0.3 in 
  Nucleotide.Matrix.pp Format.std_formatter qsim;
  Format.print_newline ();
  Format.print_newline ();
  (* A_with_gap.Matrix.pp Format.std_formatter qre; *)
  let a_matrix = Nucleotide.Matrix.init (fun i j -> A_with_gap.Matrix.get qre (A_with_gap.of_int_exn (i :> int)) (A_with_gap.of_int_exn (j :> int))) in
  let qsim_moins_qre = Nucleotide.Matrix.init (fun i j -> Nucleotide.Matrix.get qsim (i :> int) (j :> int) /. Nucleotide.Matrix.get a_matrix (i :> int) (j :> int)) in
  Nucleotide.Matrix.pp Format.std_formatter qsim_moins_qre;
  let _ = Nucleotide.Matrix.pp Format.std_formatter a_matrix in
  [%expect{||}]

let% expect_test "Testing R^eps" = 
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in
  let {insertion_rate=insertion_rate; deletion_rate=deletion_rate ; inserted_residues_proba ; _} = p1 in
  let r_eps = rate_matrix p1 in
  let q_eps = A_with_gap.Matrix.(expm (scal_mul 2. r_eps)) in 
  let insertions_rate_vec = Nucleotide.Vector.init (fun i -> insertion_rate *. inserted_residues_proba.Nucleotide.%(i)) in 
  print_string "Q^eps = \n";
  A_with_gap.Matrix.pp Format.std_formatter q_eps;
  print_newline ();
  print_string "insertions_rate_vec = \n";
  Nucleotide.Vector.pp Format.std_formatter insertions_rate_vec;
  print_newline ();
  print_string "deletion_rate = \n";
  print_float deletion_rate;
  [%expect{|
    Q^eps =
    0.303985 0.193182 0.193182 0.193182 0.116468
    0.193182 0.303985 0.193182 0.193182 0.116468
    0.193182 0.193182 0.303985 0.193182 0.116468
    0.193182 0.193182 0.193182 0.303985 0.116468
    0.145585 0.145585 0.145585 0.145585 0.417662
    insertions_rate_vec =

    deletion_rate =
    0.10.125
                                                0.125
                                                0.125
                                                0.125 |}]

let%expect_test "Testing Q^eps" = 
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in 
  let q_eps = proba_matrix p1 2. in 
  print_string "Q^eps = \n";
  A_with_gap.Matrix.pp Format.std_formatter q_eps;
  [%expect {|
    Q^eps =
    -0.116468         0         0         0  0.116468
            0 -0.116468         0         0  0.116468
            0         0 -0.116468         0  0.116468
            0         0         0 -0.116468  0.116468
    0.0291169 0.0291169 0.0291169 0.0291169 -0.116468 |}]
  

let%expect_test "Test de la matrice F84 : comparaison avec la matrice que RE08 obtienne" = 
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in
  let qre = f84_rate_matrix_nucleotide_maker p4 0.3 ~alpha:0.6 ~beta:0.4 in
  print_string "Q^eps = \n";
  Nucleotide.Matrix.pp Format.std_formatter qre;
  [%expect{||}]


let%expect_test "Testing value of the param type" = 
  let module M = Make(Nucleotide) in 
  let open M in
  let open Inference in
  let {exchangeability; stationary_distribution; insertion_rate; deletion_rate ; inserted_residues_proba; _} = p1 in 
  print_string "Exchangeability matrix : \n";
  Nucleotide.Matrix.pp Format.std_formatter exchangeability;
  print_newline ();
  print_string "Stationary distribution : \n";
  Nucleotide.Vector.pp Format.std_formatter stationary_distribution;
  print_newline ();
  print_string "Insertion rate : \n";
  Format.printf "%f" insertion_rate;
  print_newline ();
  print_string "Deletion rate : \n";
  Format.printf "%f" deletion_rate;
  print_newline ();
  print_string "Inserted residues proba : \n";
  Nucleotide.Vector.pp Format.std_formatter inserted_residues_proba;
  print_newline ();
  [%expect {|
    Exchangeability matrix :

    Stationary distribution :

    Insertion rate :

    Deletion rate :

    Inserted residues proba :
    -4  1  1  1
     1 -4  1  1
     1  1 -4  1
     1  1  1 -40.25
               0.25
               0.25
               0.250.5000000.100000
    0.25
                                   0.25
                                   0.25
                                   0.25 |}]



