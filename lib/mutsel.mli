(**
   https://www.biorxiv.org/content/biorxiv/early/2016/04/30/037689.full.pdf
   https://academic.oup.com/mbe/article/34/1/204/2656188


   The provided code implements a model for simulating the evolution of genetic sequences at the codon level.
   Codons are sequences of three nucleotides that encode specific amino acids in the genetic code.

   The code defines a parameter record (param) that represents the various factors affecting codon evolution, 
   such as mutation rates, selection pressures, and genetic biases. 

   - The `random_param` function generates random parameter values, 
     while `flat_param` creates default parameter values.

   - The `stationary_distribution` function determines the equilibrium 
     frequencies of different codons in the population. It calculates the probabilities of each codon 
     being present in the long-term stationary state, considering factors such as nucleotide distribution, 
     fitness values, and other parameters.

   - The `transition_probability_matrix` function computes the probabilities of transitioning from one codon 
     to another over a specified time period. It uses the rate matrix to calculate the probabilities of codon 
     substitutions during evolution. 

*)

module NSCodon = Codon.Universal_genetic_code.NS
module NSCodon_rate_matrix : module type of Rate_matrix.Make(NSCodon)

type param = {
  nucleotide_rates : Rate_matrix.Nucleotide.t ;
  nucleotide_stat_dist : Nucleotide.vector ;
  omega : float ; (* dN/dS *)
  scaled_fitness : Amino_acid.vector ;
  gBGC : float ;
  pps : float ; (* persistent positive selection intensity Z as in Tamuri & dos Reis 2021 *)
}


(**
   [random_param rng ~nucleotide_process:np ~alpha] generates random parameter values 
   for codon evolution simulation. It takes a random number generator [rng], a nucleotide 
   process [np], and a float value [alpha] as input and returns a parameter 
   record.
*)
val random_param :
  Gsl.Rng.t ->
  nucleotide_process:Nucleotide_process.t ->
  alpha:float ->
  param

(**
   [flat_param ()] creates default parameter values for codon evolution simulation. 
   It returns a parameter record.

   Example of usage of [flat_param] : 
   {[
     let param = flat_param () in
   ]}
*)
val flat_param : unit -> param

(**
   [rate_matrix param] computes the rate matrix for codon substitutions based on the 
   given parameter values [param]. It returns a rate matrix.   

   Example of usage of [rate_matrix] : 
   {[
     let param = flat_param () in
     let matrix = rate_matrix param in
   ]}
*)
val rate_matrix : param -> NSCodon_rate_matrix.t

(**
   [stationary_distribution param] calculates the stationary distribution of codons based on the given parameter values [param]. 
   It returns the equilibrium frequencies of different codons in the population as a vector.

   Example:
   {[
     let param = flat_param () in
     let distribution = stationary_distribution param in
   ]}    
*)
val stationary_distribution : param -> NSCodon.vector

(**
   [transition_probability_matrix param t] computes the transition probability matrix for codon substitutions 
   over a specified time period [t] based on the given parameter values [param]. 
   It returns the matrix of probabilities of transitioning from one codon to another.  

   Example:

   {[
     let param = flat_param () in
     let time = 0.1 in
     let transition_matrix = transition_probability_matrix param time in
   ]}
*)
val transition_probability_matrix : param -> float -> NSCodon.matrix
