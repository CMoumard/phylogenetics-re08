(** This module provides functionality related to codons, which are triplets of nucleotides
    that encode amino acids in the genetic code.

    A genetic code is a mapping between codons and the corresponding amino acids they encode.
    This module includes an implementation for the universal genetic code as well as functions
    to work with genetic codes specified by the NCBI ({{:https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi?mode=c}this NCBI page}).

    The module also defines types and functions to manipulate codons and nucleotides.
*)

(** {2 Codon type}

    The type representing a codon, which is a triplet of nucleotides.
*)
type t

(** {2 Genetic Code type}

    The type representing a genetic code, specified by the NCBI.

    Each genetic code is identified by a unique integer identifier and has a label describing its origin.
    It is associated with a string representing the amino acids encoded by the codons.
*)
type genetic_code

(** List of genetic codes specified by the NCBI.

    The list contains tuples with the following components:
    - Genetic code identifier (integer)
    - Genetic code label (string)
    - String representation of the amino acids encoded by the codons
*)
val genetic_codes : genetic_code list

(** Translation table of a genetic code to its identifier.

    [code] The genetic code
    Returns: The identifier of the genetic code
*)
val transl_table : genetic_code -> int

(** Get the label of a genetic code.

    [code] The genetic code
    Returns: The label of the genetic code
*)
val label_of_genetic_code : genetic_code -> string

(** {2 Codon functions}

    The following functions are available for working with codons.
*)

(** Conversion module signature for codons.

    The module signature includes functions for converting codons to and from string representations,
    finding the neighbors of a codon, and extracting the nucleotides of a codon.
*)
module type S = sig
  include Alphabet.S_int

  (** Convert a codon to its string representation.

      [c] The codon
      Returns: The string representation of the codon

      Example:

      {[ 
        let codon_option = Codon.of_string "ATG" in
        match codon_option with
        | Some codon -> assert (Codon.to_string codon = "ATG")
        | None -> failwith "Invalid codon string"
      ]}

      In this example, the codon is converted to its string representation. The string representation
      is then compared to the expected value.
  *)
  val to_string : t -> string

  (** Convert a string representation to a codon.

      [s] The string representation of a codon
      Returns: The codon, or None if the string is not a valid codon
  *)
  val of_string : string -> t option

  (** Find the neighbors of two codons.

      [p] The first codon
      [q] The second codon
      Returns: The index of the differing nucleotide and the nucleotides themselves,
               or None if the codons are not neighbors

      Example:


      {[ 
        let codon_p = Codon.of_string "ATA" in
        let codon_q = Codon.of_string "ATG" in
        match Codon.neighbours codon_p codon_q with
        | Some (index, nucleotide_p, nucleotide_q) ->
          assert (index = 2);
          assert (nucleotide_p = Nucleotide.A);
          assert (nucleotide_q = Nucleotide.G)
        | None -> failwith "Codons are not neighbors"
      ]}

      In this example, the codons are compared to find the index of the differing nucleotide
      and the nucleotides themselves. The index and nucleotides are then compared to the expected values.

  *)
  val neighbours : t -> t -> (int * Nucleotide.t * Nucleotide.t) option

  (** Extract the nucleotides of a codon.

      [c] The codon
      Returns: The three nucleotides of the codon

      Example : 

      {[ 
        let codon_option = Codon.of_string "ATG" in
        match codon_option with
        | None -> failwith "Invalid codon string"
        | Some codon ->
          let (n1, n2, n3) = Codon.nucleotides codon in
          assert (n1 = Nucleotide.A);
          assert (n2 = Nucleotide.T);
          assert (n3 = Nucleotide.G)
      ]}

      In this example, the nucleotides of the codon are extracted and compared to the expected values.
  *)
  val nucleotides : t -> Nucleotide.t * Nucleotide.t * Nucleotide.t
end

(** {2 Genetic Code module}

    The module type for genetic codes.

    A genetic code module provides functions to work with a specific genetic code,
    including checking for stop codons, retrieving the amino acid encoded by a codon,
    and checking if two codons are synonymous.

    The module also includes a nested module for the non-stop codons subset.
*)
module type Genetic_code = sig
  type codon = t

  (** List of stop codons. *)
  val stop_codons : codon list

  (** Check if a codon is a stop codon.

      [c] The codon
      Returns: True if the codon is a stop codon, false otherwise

      Example :

      {[ 
        let codon_option = Codon.of_string "TAA" in
        match codon_option with
        | None -> failwith "Invalid codon string"
        | Some codon ->
          assert (Codon.is_stop_codon codon = true)
      ]}


      A stop codon, also known as termination codon or nonsense codon, is a 
      specific codon in the genetic code that signals the termination of 
      protein synthesis. When a stop codon is encountered during translation, 
      the ribosome releases the newly synthesized polypeptide chain and the 
      translation process comes to a halt. In the universal genetic code, 
      there are three stop codons: "TAA", "TAG", and "TGA". These codons do 
      not encode any amino acid and serve as termination signals for the 
      protein synthesis machinery.
  *)
  val is_stop_codon : codon -> bool

  (** Get the amino acid encoded by a codon.

      [c] The codon
      Returns: The amino acid encoded by the codon, or None if no matching amino acid is found

      {[ 
        let codon = Codon.of_string "ATG" in
        let amino_acid_option = Codon.aa_of_codon codon in
        match amino_acid_option with
        | Some amino_acid -> assert (amino_acid = Amino_acid.Met)
        | None -> failwith "No matching amino acid found"
      ]}
  *)
  val aa_of_codon : codon -> Amino_acid.t option

  (** Check if two codons are synonymous.

      [c1] The first codon
      [c2] The second codon
      Returns: True if the codons are synonymous, false otherwise


      Example :

      {[ 
        let codon1_option = Codon.of_string "ATG" in
        let codon2_option = Codon.of_string "ATA" in
        match codon1_option, codon2_option with
        | None, _ | _, None -> failwith "Invalid codon string"
        | Some codon1, Some codon2 ->
          assert (Codon.synonym codon1 codon2 = false)
      ]}

      In this example, the codons are compared to check if they are synonymous.
      Two codons are synonymous if they encode the same amino acid.
  *)
  val synonym : codon -> codon -> bool

  (** {3 Non-stop Codons module}

      The module type for the non-stop codons subset.

      This module provides functions specific to the non-stop codons subset of a genetic code,
      including retrieving the amino acid encoded by a codon (without the need for option type)
      and converting an integer to a codon.

      The functions in this module assume that the given codon is a non-stop codon.
  *)
  module NS : sig
    include S

    (** Convert a codon to the type 'codon'.

        [c] The codon
        Returns: The codon of type 'codon'

        Example :

        {[ 
          let codon = Codon.NS.to_codon (Nucleotide.A, Nucleotide.T, Nucleotide.G) in
          assert (Codon.to_string codon = "ATG")
        ]}
    *)
    val to_codon : t -> codon

    (** Get the amino acid encoded by a codon.

        [c] The codon
        Returns: The amino acid encoded by the codon
        Raises: Invalid_argument if no matching amino acid is found

        Exemple :

        {[ 
          let codon = Codon.NS.to_codon (Nucleotide.A, Nucleotide.T, Nucleotide.G) in
          let amino_acid = Codon.NS.aa_of_codon codon in
          assert (amino_acid = Amino_acid.Met)
        ]}
    *)
    val aa_of_codon : t -> Amino_acid.t

    (** Check if two codons are synonymous.

        [c1] The first codon
        [c2] The second codon
        Returns: True if the codons are synonymous, false otherwise

        Exemple :

        {[ 
          let codon1 = Codon.NS.to_codon (Nucleotide.A, Nucleotide.T, Nucleotide.G) in
          let codon2 = Codon.NS.to_codon (Nucleotide.A, Nucleotide.T, Nucleotide.A) in
          assert (Codon.NS.synonym codon1 codon2 = false)
        ]}
    *)
    val synonym : t -> t -> bool

    (** Convert an integer to a codon.

        [n] The integer
        Returns: The codon
        Raises: Invalid_argument if the integer is invalid
    *)
    val of_int_exn : int -> t
  end
end

(** Universal genetic code module.

    This module provides an implementation of the universal genetic code,
    which is the default genetic code used in most organisms.

    The universal genetic code consists of 64 codons, out of which 61 encode specific amino acids,
    and the remaining 3 codons serve as stop signals, indicating the termination of protein synthesis.
    The specific mapping between codons and amino acids is the same across different species,
    allowing genetic information to be accurately transferred and translated into proteins.

    This module provides an implementation of the universal genetic code, including functions
    to check for stop codons, retrieve the amino acid encoded by a codon, and determine if two codons
    are synonymous.
*)
module Universal_genetic_code : Genetic_code

(** Get the implementation module for a genetic code.

    [code] The genetic code
    Returns: The implementation module for the genetic code

    {[ 
      let module Genetic_code = Codon.genetic_code_impl genetic_code in
      let stop_codons = Genetic_code.stop_codons in
      assert (List.length stop_codons = 3)
    ]}
*)
val genetic_code_impl : genetic_code -> (module Genetic_code)
