open Core

type 'a t = {
  levels : 'a array ;
  equal : 'a -> 'a -> bool ;
}

let of_list all_values ~compare =
  let levels =
    List.dedup_and_sort all_values ~compare
    |> List.to_array in
  let equal x y = compare x y = 0 in
  { levels ; equal }

let of_tree ~root_trait ~branch_trait ~leaf_trait ~trait_compare tree =
  of_list ~compare:trait_compare (
    Tree.prefix_traversal tree ~init:[root_trait]
      ~branch:(fun acc bi -> branch_trait bi :: acc)
      ~leaf:(fun acc li -> leaf_trait li :: acc)
      ~node:Fn.const
  )


let of_annotated_tree (tree : 'a Annotated_tree.t) ~compare =
  of_list ~compare (
    Tree.prefix_traversal tree ~init:[]
      ~branch:Fun.const
      ~leaf:(fun acc li -> li.Annotated_tree.trait :: acc)
      ~node:(fun acc n  -> n.Annotated_tree.trait :: acc)
  )

let of_alignment al =
  Alignment.fold al ~init:[] ~f:(fun acc ~description ~sequence:_ ->
      description :: acc
    )
  |> of_list ~compare:String.compare

let level factor c =
  Array.find_mapi factor.levels ~f:(fun i x ->
      if factor.equal x c then Some i else None
    )

let level_exn factor c =
  Array.find_mapi_exn factor.levels ~f:(fun i x ->
      if factor.equal x c then Some i else None
    )

let cardinal factor = Array.length factor.levels

let to_list factor = Array.to_list factor.levels

let to_array { levels ; _ } = levels
