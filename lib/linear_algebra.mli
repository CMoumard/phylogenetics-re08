(** This module provides linear algebra functions for vectors and matrices. *)

(** A vector of floats. *)
module type Vector = sig
  type t

  (** [length v] returns the length of the vector [v]. *)
  val length : t -> int

  val to_bigarray_array1 :
           t ->
           (float, Bigarray.float64_elt, Bigarray.fortran_layout)
           Bigarray.Array1.t
  (** [to_bigarray_array1 v] converts the vector [v] to a Bigarray.Array1.t. *)

  val of_bigarray_array1 :
    (float, Bigarray.float64_elt, Bigarray.fortran_layout) Bigarray.Array1.t -> t
  (** [of_bigarray_array1 v] converts the Bigarray.Array1.t to a vector. *)
  
    

  
  (** [init n ~f] initializes a vector of length [n] by applying the function [f] to each index. *)
  val init : int -> f:(int -> float) -> t

  (** [map v ~f] applies the function [f] element-wise to the vector [v]. *)
  val map : t -> f:(float -> float) -> t

  (** [inplace_scal_mul a v] multiplies the vector [v] by the scalar [a] in-place. *)
  val inplace_scal_mul: float -> t -> unit

  (** [scal_mul a v] multiplies the vector [v] by the scalar [a] and returns a new vector. *)
  val scal_mul : float -> t -> t

  (** [scal_add a v] adds the scalar [a] to each element of the vector [v] and returns a new vector. *)
  val scal_add: float -> t -> t

  (** [add v1 v2] adds the vectors [v1] and [v2] element-wise and returns a new vector. *)
  val add : t -> t -> t

  (** [mul v1 v2] computes the element-wise product of vectors [v1] and [v2] and returns a new vector. *)
  val mul : t -> t -> t

  (** [sum v] computes the sum of all elements in the vector [v]. *)
  val sum : t -> float

  (** [log v] computes the element-wise logarithm of the vector [v] and returns a new vector. *)
  val log : t -> t

  (** [exp v] computes the element-wise exponential of the vector [v] and returns a new vector. *)
  val exp : t -> t

  (** [min v] returns the minimum element in the vector [v]. *)
  val min : t -> float

  (** [max v] returns the maximum element in the vector [v]. *)
  val max : t -> float

  (** [get v i] returns the element at index [i] in the vector [v]. *)
  val get : t -> int -> float

  (** [set v i x] sets the element at index [i] in the vector [v] to the value [x]. *)
  val set : t -> int -> float -> unit

  (** [robust_equal ~tol v1 v2] compares two vectors [v1] and [v2] and returns true if they are equal up to a relative difference of [tol]. *)
  val robust_equal : tol:float -> t -> t -> bool

  (** [of_array arr] creates a vector from a float array [arr]. *)
  val of_array : float array -> t

  (** [to_array v] converts the vector [v] to a float array. *)
  val to_array : t -> float array

  (** [pp fmt v] pretty-prints the vector [v] to the formatter [fmt]. *)
  val pp : Format.formatter -> t -> unit

  (** [sub v start_idx end_idx] returns a subvector of [v] from [start_idx] to [end_idx] *)
  val sub : t -> int -> int -> t

  (** [dot vect1 vect2] returns the dot product of [vect1] and [vect2] *)
  val dot : t -> t -> float 
end

(** A square matrix of floats. *)
module type Matrix = sig
  type vec
  type t

  (** [dim m] returns the dimensions (rows, columns) of the matrix [m]. *)
  val dim : t -> int * int

  (** {5 Matrix and vector creation} *)

  (** [init n ~f] initializes a square matrix of size [n]x[n] by applying the function [f] to each row and column index. *)
  val init : int -> f:(int -> int -> float) -> t

  (** [init_sym n ~f] initializes a symmetric square matrix of size [n]x[n] by applying the function [f] to elements where [i <= j]. *)
  val init_sym : int -> f:(int -> int -> float) -> t

  (** [diagm v] creates a square diagonal matrix from the vector [v] by placing its elements on the main diagonal. *)
  val diagm : vec -> t

  (** [mul a b] is the element-wise multiplication of [a] and [b].*)
  val mul : 
    t -> t -> t


  (** [add m1 m2] adds matrices [m1] and [m2] element-wise and returns a new matrix. *)
  val add : t -> t -> t

  (** [scal_mul a m] multiplies the matrix [m] by the scalar [a] and returns a new matrix. *)
  val scal_mul : float -> t -> t

  (** [inplace_scal_mul a m] multiplies the matrix [m] by the scalar [a] in-place. *)
  val inplace_scal_mul: float -> t -> unit

  (** [dot ?transa ?transb m1 m2] computes the matrix product of matrices [m1] and [m2] with optional transposition and returns a new matrix. *)
  val dot :
    ?ar:int ->
    ?ac:int ->
    ?br:int ->
    ?bc:int ->
    ?transa:[`N | `T] ->
    ?transb:[`N | `T] ->
    t -> t -> t

  (** [apply ?trans m v] computes the matrix-vector product of matrix [m] and vector [v] with optional transposition and returns the resulting vector. *)
  val apply : ?trans:[`N | `T] -> t -> vec -> vec

  (** [pow m k] computes the matrix [m] raised to the power [k] and returns a new matrix. *)
  val pow : t -> int -> t

  (** [expm m] computes the matrix exponential of matrix [m] and returns a new matrix. *)
  val expm : t -> t

  (** [log m] computes the element-wise logarithm of matrix [m] and returns a new matrix. *)
  val log : t -> t

  (** [robust_equal ~tol m1 m2] compares two matrices [m1] and [m2] and returns true if they are equal up to a relative difference of [tol]. *)
  val robust_equal : tol:float -> t -> t -> bool

  (** [get m i j] returns the element at row [i] and column [j] in the matrix [m]. *)
  val get : t -> int -> int -> float

  (** [set m i j x] sets the element at row [i] and column [j] in the matrix [m] to the value [x]. *)
  val set : t -> int -> int -> float -> unit

  (** [row m i] returns a copy of row [i] from the matrix [m] as a vector. *)
  val row : t -> int -> vec

  (** [diagonalize m] diagonalizes the matrix [m] such that M = PxDxP^T and returns the diagonal vector [D] and the matrix [P]. *)
  val diagonalize : t -> vec * t

  (** [transpose m] transposes the matrix [m] and returns a new matrix. *)
  val transpose : t -> t  

  (** [inverse m] computes the inverse of matrix [m] and returns a new matrix. *)
  val inverse: t -> t

  (** [zero_eigen_vector m] returns a vector [v] such that Vec.sum v = 1 and mat_vec_mul m v = zero. *)
  val zero_eigen_vector : t -> vec

  (** [of_arrays a] creates a matrix from a 2D float array [a]. Returns an option that is [Some m] if the array has a rectangular shape and [None] otherwise. *)
  val of_arrays : float array array -> t option

  (** [of_arrays_exn a] creates a matrix from a 2D float array [a]. Raises an exception if the array does not have a rectangular shape. *)
  val of_arrays_exn : float array array -> t

  (** [pp fmt m] prints the matrix [m] to the formatter [fmt]. *)
  val pp : Format.formatter -> t -> unit
end

module type S = sig
  type vec
  type mat

  module Vector : Vector with type t = vec
  module Matrix : Matrix with type t = mat and type vec := vec
end




include S with type mat = private Lacaml.D.mat
           and type vec = private Lacaml.D.vec


val dotv : ?length:int -> mat -> vec -> vec
(**[dotv ?length m v] computes the matrix-vector product of matrix [m] and vector [v] with optional arguments [length], which is the length of the result vector (an error is raised if the length is not correct, i.e. if [length <> Matrix.dim m |> snd]). *)

(** Example usage of functions in the Vector module:

    {[
      let v = Vector.init 5 ~f:(fun i -> float_of_int (i + 1)) (* Creates a vector [1.0; 2.0; 3.0; 4.0; 5.0] *)

      let v_length = Vector.length v (* Returns the length of the vector [5] *)

      let v_scaled = Vector.scal_mul 2.0 v (* Multiplies each element of the vector by 2.0 and returns the resulting vector *)

      let v_sum = Vector.sum v (* Computes the sum of the elements of the vector and returns the result [15.0] *)

      let v_get_2 = Vector.get v 2 (* Returns the element at index 2 of the vector and returns the result [3.0] *)

      let v_set_3 = Vector.set v 3 10.0 (* Sets the element at index 3 of the vector to 10.0 *)

      let v_robust_equal = Vector.robust_equal ~tol:1e-6 v v_scaled (* Compares two vectors and returns true if they are equal up to a relative difference of 1e-6 *)

      let v_array = Vector.to_array v (* Converts the vector to a float array *)

          Vector.pp Format.std_formatter v (* Prints the vector to the standard output *)

      let mapped_v = Vector.map v ~f:Float.sqrt (* Applies the square root function to each element of the vector and returns the resulting vector *)

      let logged_v = Vector.log v (* Computes the element-wise logarithm of the vector and returns the resulting vector *)

      let exponentiated_v = Vector.exp v (* Computes the element-wise exponential of the vector and returns the resulting vector *)

      let v_min = Vector.min v (* Computes the minimum element in the vector *)

      let v_max = Vector.max v (* Computes the maximum element in the vector *)
    ]}
*)

(** Example usage of functions in the Matrix module:

    {[
      let m = Matrix.init 3 ~f:(fun i j -> float_of_int (i + j)) (* Creates a matrix
                                                                    | 0.0 1.0 2.0 |
                                                                    | 1.0 2.0 3.0 |
                                                                    | 2.0 3.0 4.0 | *)

      let m_dim = Matrix.dim m (* Returns the dimensions of the matrix (3, 3) *)

      let m_transposed = Matrix.transpose m (* Transposes the matrix and returns the resulting matrix *)

      let m_inverse = Matrix.inverse m (* Computes the inverse of the matrix and returns the resulting matrix *)

      let m_row_1 = Matrix.row m 1 (* Returns the second row of the matrix as a vector *)

      let m_get_2_2 = Matrix.get m 2 2 (* Returns the element at row 2 and column 2 of the matrix *)

      let m_set_1_1 = Matrix.set m 1 1 10.0 (* Sets the element at row 1 and column 1 of the matrix to 10.0 *)

      let m_mul = Matrix.mul m m_transposed (* Computes the element-wise product of two matrices and returns the resulting matrix *)

      let m_add = Matrix.add m m_transposed (* Adds two matrices element-wise and returns the resulting matrix *)

      let m_scal_mul = Matrix.scal_mul 2.0 m (* Multiplies the matrix by a scalar and returns the resulting matrix *)

      let m_dot = Matrix.dot m m_transposed (* Computes the matrix product of two matrices and returns the resulting matrix *)

      let m_pow = Matrix.pow m 3 (* Computes the matrix raised to the power 3 and returns the resulting matrix *)

      let m_expm = Matrix.expm m (* Computes the matrix exponential and returns the resulting matrix *)

      let m_log = Matrix.log m (* Computes the element-wise logarithm of the matrix and returns the resulting matrix *)

      let m_robust_equal = Matrix.robust_equal ~tol:1e-6 m m_transposed (* Compares two matrices and returns true if they are equal up to a relative difference of 1e-6 *)

      let m_diagm = Matrix.diagm (Vector.of_array [|1.0; 2.0; 3.0|]) (* Creates a diagonal matrix from a vector *)

      Matrix.pp Format.std_formatter m (* Prints the matrix to the standard output *)
    ]}
*)




