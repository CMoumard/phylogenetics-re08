open Core

type 'a node_info = { trait : 'a; }
[@@deriving sexp]
type 'a leaf_info = { name : string; trait : 'a; }
and branch_info = { length : float; }
and 'a t = ('a node_info, 'a leaf_info, branch_info) Tree.t
[@@deriving sexp]

type 'a branch = ('a node_info, 'a leaf_info, branch_info) Tree.branch

let binary_node trait x y =
  Tree.binary_node { trait } x y

let branch length x = Tree.branch { length } x

let leaf ~name ~trait = Tree.leaf { name ; trait }

let map t ~f =
  Tree.map t
    ~node:(fun (ni : _ node_info) -> { trait = f ni.trait })
    ~leaf:(fun (li : _ leaf_info) -> { li with trait = f li.trait })
    ~branch:Fn.id

let map_branches (t:_ t) ~node ~leaf ~branch =
  let root_data = function
    | Tree.Leaf l -> leaf l
    | Tree.Node n -> node n.data
  in
  let rec traverse_tree = function
    | Tree.Leaf _ as l -> l
    | Tree.Node n ->
      Tree.node n.data (List1.map n.branches ~f:(traverse_branch n.data))
  and traverse_branch parent_data (Branch bi) =
    Tree.branch
      (branch (node parent_data) bi.data (root_data bi.tip))
      (traverse_tree bi.tip)
  in
  traverse_tree t

let clear (t: _ t) =
  Tree.map t
    ~node:(Fn.const ())
    ~branch:(fun { length } -> length)
    ~leaf:(fun { name ; _ } -> name )

let parse_float (tree: string t) =
  let exception Non_float_trait of string in
  try
    map tree ~f:(fun str ->
        match float_of_string_opt str with
        | Some x -> x
        | None -> raise (Non_float_trait str)
      )
    |> Result.return
  with (Non_float_trait trait) ->
    let msg = sprintf "Failed to convert trait '%s' to string" trait in
    Error (`Msg msg)

let default_trait_label = "Trait"

let to_newick ?(trait_label = default_trait_label) (tree : 'a t) =
  let rec node ?parent_branch = function
    | Tree.Node { data : 'a node_info ; branches } -> Newick.{
        name = None ;
        tags = [trait_label, data.trait] ;
        parent_branch ;
        children = List1.map branches ~f:branch |> List1.to_list
      }
    | Tree.Leaf (l:'a leaf_info) -> Newick.{
        name = Some (l.name) ;
        tags = [trait_label, l.trait] ;
        parent_branch ;
        children = [] ;
      }
  and branch (Branch b) = node ~parent_branch:(b.data.length) b.tip
  in
  node tree

let clip_tree_on_alignment tree ali =
  let ali_species = List.init (Alignment.nrows ali) ~f:(Alignment.description ali) in
  let clipped_tree = Tree.leafset_generated_subtree tree (fun { name ; _ } -> Some name) ali_species in
  match clipped_tree with
  | Some tree -> Result.Ok tree
  | None -> Rresult.R.error_msgf "Not a single matching species name between tree and alignment"

let root_trait : 'a t -> 'a = function
  | Node n -> n.data.trait
  | Leaf l -> l.trait

let trait_of_tags tags ~trait_label =
  List.Assoc.find tags ~equal:String.equal trait_label
  |> Result.of_option ~error:(
    `Improper_annotation (
      sprintf "no tag with name '%s' : available tags are [%s]"
        trait_label
        (List.map tags ~f:fst |> String.concat ~sep:";")
    )
  )

let of_newick ?(trait_label = default_trait_label) (newick : Newick.t) =
  let open Let_syntax.Result in
  let rec branch ({ parent_branch ; _ } as tree : Newick.t) =
    let* length = Result.of_option parent_branch ~error:(`Improper_annotation "missing branch length") in
    let+ tip = node tree in
    Tree.branch {length} tip
  and node ({name ; tags ; children ; _} : Newick.t) =
    let* trait = trait_of_tags tags ~trait_label in
    match children with
    | [] ->
      let+ name = Result.of_option name ~error:(`Improper_annotation "unnamed leaf") in
      Tree.leaf { name ; trait }
    | children ->
      let+ branches = List.map children ~f:branch |> Result.all in
      Tree.node { trait } (List1.of_list_exn branches)
  in
  node newick

let from_file ?trait_label fn =
  let open Let_syntax.Result in
  let* newick = Newick.from_file fn in
  match of_newick newick ?trait_label with
  | Ok x -> Ok x
  | Error (`Improper_annotation e) -> Error (`Improper_annotation e)

let from_file_exn ?trait_label fn =
  match from_file ?trait_label fn with
  | Ok x -> x
  | Error (`Improper_annotation e) ->
    failwithf "Tree in file %s has improper annotation : %s" fn e ()
  | Error (`Newick_parser_error _ as e) ->
    failwithf "Failed to parse tree file %s as Newick : %s"
      fn (Newick.string_of_error e) ()

let pair_tree ~branch_length1 ~branch_length2 ~npairs =
  let leaf i trait = Tree.Leaf { name = sprintf "%d" i ; trait } in
  let branch length tip = Tree.branch { length } tip in
  let tree = Tree.binary_node {trait=`Ancestral} in
  let make_pair i =
    tree
      (branch branch_length2 (leaf (2 * i) `Ancestral))
      (branch branch_length2 (leaf (2 * i + 1) `Convergent))
    |> branch branch_length1
  in
  Tree.node {trait=`Ancestral} (List1.init npairs ~f:make_pair)

let trait_values_uniq (tree : _ t) =
  Tree.prefix_traversal tree ~init:Set.Poly.empty
    ~branch:Fn.const
    ~leaf:(fun values l -> Set.Poly.add values l.trait)
    ~node:(fun values n -> Set.Poly.add values n.trait)
  |> Set.Poly.to_array

let trait_values (tree : _ t) =
  Tree.prefix_traversal tree ~init:[]
    ~branch:Fn.const
    ~leaf:(fun values l -> l.trait::values)
    ~node:(fun values n -> n.trait::values)

module Discrete_traits = struct

  let infer_conditions_parsimony tree ~species_traits : string t =
    let trait_levels =
      String.Map.data species_traits
      |> String.Set.of_list
      |> String.Set.to_array
    in
    let trait_map =
      String.Set.of_array trait_levels
      |> String.Set.to_list
      |> List.mapi ~f:(fun i x -> x, i)
      |> String.Map.of_alist_exn
    in
    let category l =
      String.Map.find_exn species_traits l
      |> String.Map.find trait_map
    in
    let n = Array.length trait_levels in
    let cost x y = if x <> y then 1. else 0. in
    Fitch.fitch ~cost ~n ~category tree
    |> Tree.map
      ~node:(fun (_, cond) -> { trait = trait_levels.(cond)})
      ~leaf:(fun (l, cond) -> { name = l ; trait = trait_levels.(cond) })
      ~branch:(fun length -> { length })

  let simulate ~rng tree ~length ~leaf_name ~mu : ([`BG | `FG] t) =
    Tree.propagate tree ~init:`BG
      ~node:(fun trait _ -> trait, { trait })
      ~leaf:(fun trait l -> { trait ; name=leaf_name l })
      ~branch:(fun trait bi ->
          let length = length bi in
          let trait = match trait with
            | `FG -> `FG
            | `BG ->
              if Float.(Gsl.Randist.exponential rng ~mu < length)
              then `FG
              else `BG
          in
          trait, { length }
        )

end

module Brownian = struct

  open Lacaml.D

  type indexed_tree = {
    tree : (int, int*string, float) Tree.t ;
    leaf_count : int ;
    node_count : int ;
  }

  (** generate ids starting from 1 to play nice with Lacaml *)
  let id_gen () =
    let i = ref 0 in
    (fun () -> incr i ; !i), fun () -> !i


  let index_nodes tree : indexed_tree =
    let next_node, current_node = id_gen () in
    let next_leaf, current_leaf = id_gen () in
    let tree = Tree.map tree
        ~node:(fun _ -> next_node ())
        ~leaf:(fun b -> next_leaf (), b)
        ~branch:Fn.id in
    {
      tree ;
      leaf_count = current_leaf () ;
      node_count = current_node () ;
    }

  let compute_distances { tree ; leaf_count ; node_count } =
    let inner_distances = Mat.make0 node_count node_count in
    let leaves_distances = Mat.make0 node_count leaf_count in
    let rec node t = (match t with
        | Tree.Node n -> List1.iter n.branches ~f:(branch n.data)
        | Leaf _ -> ());
    and branch origin (Branch b) =
      (match b.tip with
       | Tree.Node n -> inner_distances.{origin, n.data} <- 1. /. b.data
       | Leaf (id, _name) -> leaves_distances.{origin, id} <- 1. /. b.data
      );
      node b.tip
    in node tree;
    inner_distances,
    leaves_distances

  let coefficient_matrix inner_distances leaves_distances =
    let n = Mat.dim1 inner_distances in
    let inner_distances_trans = Mat.transpose_copy inner_distances in
    let leaves_distances_trans = Mat.transpose_copy leaves_distances in
    Mat.init_rows n n (fun row col ->
        if row = col then
          (Mat.col inner_distances row |> Vec.sum) +.
          (Mat.col inner_distances_trans row |> Vec.sum) +.
          (Mat.col leaves_distances_trans row |> Vec.sum)
        else
          -. (
            inner_distances.{row,col} +.
            inner_distances.{col,row}
          )
      )

  let constant_matrix indexed_tree ~leaves_distances ~trait_values =
    let leaves =
      Tree.leaves indexed_tree
      |> List.sort ~compare:(fun (id_a, _) (id_b, _) -> Int.compare id_a id_b) in
    let leaf_traits =
      List.map leaves ~f:(fun (_id, name) ->
          [String.Map.find_exn trait_values name]
        )
      |> Lacaml.D.Mat.of_list in
    Lacaml.D.gemm leaves_distances leaf_traits

  let linear_system ({ tree ; _ } as indexed_tree) ~trait_values =
    let inner_distances, leaves_distances = compute_distances indexed_tree in
    let coefs = coefficient_matrix inner_distances leaves_distances in
    let constant = constant_matrix tree ~leaves_distances ~trait_values in
    coefs, constant

  let infer_ancestral_traits tree ~trait_values : float t =
    let indexed_tree = index_nodes tree in
    let coefs, vec = linear_system indexed_tree ~trait_values in
    getrs coefs vec;
    let traits_vector = Mat.as_vec vec in
    Tree.map indexed_tree.tree
      ~node:(fun i -> { trait = traits_vector.{i} })
      ~leaf:(fun (_, name) ->
          {
            trait = String.Map.find_exn trait_values name;
            name ;
          })
      ~branch: (fun length -> { length })

  let simulate ?(scale=1.) tree ~rng ~root_value : float t =
    let rec node t ~trait =
      match t with
      | Tree.Node n -> Tree.node { trait } (List1.map n.branches ~f:(branch ~trait))
      | Tree.Leaf l -> Tree.leaf { name = l ; trait}
    and branch (Tree.Branch bi) ~trait =
      let length = bi.data in
      let divergence = Gsl.Randist.gaussian rng ~sigma:(Float.sqrt length *. scale) in
      let tip_value = trait +. divergence in
      Tree.branch { length } (node bi.tip ~trait:tip_value)
    in
    node tree ~trait:root_value


  let%expect_test "inference on tree with two branches" =
    let tree = Tree.node ()
        (List1.of_list_exn Tree.[
             branch 0.5 (Tree.leaf "A") ;
             branch 1. (Tree.leaf "B") ;
           ])
    in
    let trait_values = String.Map.of_alist_exn [ "A", 15. ; "B", 0. ; ] in
    let annotated_tree = infer_ancestral_traits tree ~trait_values in
    let newick = map annotated_tree ~f:string_of_float |> to_newick in
    let newick_str = Newick.to_string newick in
    print_endline newick_str;
    [%expect {| (A:0.5[&&NHX:Trait=15.],B:1[&&NHX:Trait=0.])[&&NHX:Trait=10.]; |}]

  let%expect_test "inference on small non-trivial tree" =
    let nodeX = Tree.node ()
        (List1.of_list_exn Tree.[
             branch 0.5 (Tree.leaf "A");
             branch 1. (Tree.leaf "B");
           ])
    in
    let tree = Tree.node ()
        (List1.of_list_exn Tree.[
             branch 0.3 nodeX ;
             branch 0.5 (Tree.leaf "C");
           ])
    in
    let trait_values = String.Map.of_alist_exn [ "A", 15. ; "B", 0. ; "C", 30.] in
    let annotated_tree = infer_ancestral_traits tree ~trait_values in
    let newick = map annotated_tree ~f:string_of_float |> to_newick in
    let newick_str = Newick.to_string newick in
    print_endline newick_str;
    [%expect {| ((A:0.5[&&NHX:Trait=15.],B:1[&&NHX:Trait=0.]):0.3[&&NHX:Trait=15.8823529412],C:0.5[&&NHX:Trait=30.])[&&NHX:Trait=21.1764705882]; |}]
end


module Draw = struct

  type rgb = int * int * int
  let color_of_rgb (r,g,b) = Gg.Color.v_srgbi r g b
  let rgb_palette : rgb array = [|
    0,0,0 ;
    248,47,101;
    177,230,50;
    110,193,248;
    162,37,226;
    55,136,17;
    222,136,106;
    67,70,171;
  |]

  let palette = Array.map rgb_palette ~f:color_of_rgb

  let id_of_trait values c =
    Array.findi values ~f:(fun _ v -> String.(v = c))
    |> Option.map ~f:fst

  let color_of_trait ~trait_values c =
    let id =
      id_of_trait trait_values c
      |> Option.value_exn ~message:(sprintf "Trait %s does not exist" c)
    in
    try palette.(id)
    with _ -> failwithf "Color palette is not large enough to pick a color for trait %s with id %d" c id ()

  let draw (tree : _ t) ~file ~mode =
    let module Draw = Biotk.Phylo_tree_draw in
    let trait_values = trait_values_uniq tree in
    let rec node n =
      let col = color_of_trait ~trait_values in
      match n with
      | Tree.Leaf l -> Draw.leaf l.name ~col:(col l.trait)
      | Tree.Node { branches; _ } ->
        List1.to_list branches |> List.map ~f:branch |> Draw.node
    and branch (Tree.Branch {data={length}; tip}) =
      Draw.branch length (node tip)
    in
    Biotk_croquis.Croquis.(
      render (Draw.draw_tree @@ node tree) mode (`File file)
    )
end

module Discretization = struct

  let make_partitioner ~breaks =
    match List.length breaks with
    | 0 -> Error `Empty_breaks
    | n ->
      let sorted_breaks = Float.(List.sort breaks ~compare) in
      Ok (fun trait ->
          List.find_mapi sorted_breaks
            ~f:(fun i x -> Option.some_if Float.(trait <= x) i)
          |> Option.value ~default:n
        )

  let quantile_partitioner traits ~p =
    let sorted_traits =
      List.sort traits ~compare:Float.compare
      |> Array.of_list in
    let breaks = List.map p ~f:(Gsl.Stats.quantile_from_sorted_data sorted_traits)
    in
    make_partitioner ~breaks

end
