(** Nucleotide IUPAC symbol *)

include Alphabet.S_int

(** [of_char c] returns the IUPAC symbol corresponding to the given character [c].
    It returns [None] if the character is not a valid IUPAC nucleotide symbol.

    A valid IUPAC nucleotide symbol is any uppercase letter from 'A' to 'Z',
    representing one of the possible nucleotide bases (A, C, G, T, or the ambiguous symbols).

    Example:
    {[
      let symbol = Iupac_nucleotide.of_char 'A' in
      (* symbol = Some 0 *)
    ]}
*)
val of_char : char -> t option

(** [is_ambiguous sym] checks if the given IUPAC symbol [sym] represents an ambiguous nucleotide.
    It returns [true] if the symbol is ambiguous, and [false] otherwise.

    A symbol is considered ambiguous if its numerical value is greater than 3.

    Example:
    {[
      let ambiguous = Iupac_nucleotide.is_ambiguous 4 in
      (* ambiguous = true *)
    ]}
*)
val is_ambiguous : t -> bool
