(** This module provides functions and types for linear algebra operations and probabilistic computations in the context of continuous-time Markov chains (CTMCs).

    It includes functions for matrix decomposition, pruning (the Felsenstein algorithm), conditional likelihoods, conditional simulation, and path sampling. 

*)


open Linear_algebra

type matrix_decomposition = [
  | `Mat of mat
  | `Transpose of mat
  | `Diag of vec
] list
(** The type [matrix_decomposition] represents a list of matrix decompositions. It is used to describe the decomposition of a matrix into different components.

    - The constructor [`Mat of mat] represents a matrix without any decomposition.
    - The constructor [`Transpose of mat] represents the transpose of a matrix.
    - The constructor [`Diag of vec] represents a diagonal matrix.

    This type is used as an input for the `transition_probabilities` parameter in the pruning functions, where different decompositions of a matrix can be specified to model different aspects of the continuous-time Markov chain (CTMC).

    Example usage:
    {[
      let decomposition = [
        `Mat matrix1;
        `Transpose matrix2;
        `Diag vector1;
      ] in

      let result = matrix_decomposition_reduce ~dim:3 decomposition in
      (* process_result result ...*)
    ]}
*)



val matrix_decomposition_reduce : dim:int -> matrix_decomposition -> mat
(** [matrix_decomposition_reduce dim md] reduces the matrix decomposition [md]
    into a single matrix of dimension [dim]. *)

type shifted_vector = SV of vec * float

val pruning :
  ('n, 'l, 'b) Tree.t ->
  nstates:int ->
  transition_probabilities:('b -> matrix_decomposition) ->
  leaf_state:('l -> int) ->
  root_frequencies:vec ->
  float
(** [pruning t ~nstates ~transition_probabilities ~leaf_state
    ~root_frequencies] returns the probability of observing the states
    returned by [leaf_state] at the leaves of [t] given the CTMC
    specified by [nstates], [transition_probabilities] and
    [root_frequencies].
*)

val pruning_with_missing_values :
  ('n, 'l, 'b) Tree.t ->
  nstates:int ->
  transition_probabilities:('b -> matrix_decomposition) ->
  leaf_state:('l -> int option) ->
  root_frequencies:vec ->
  float
(** [pruning_with_missing_values t ~nstates ~transition_probabilities ~leaf_state
    ~root_frequencies] returns the probability of observing the states
    returned by [leaf_state] at the leaves of [t] given the CTMC
    specified by [nstates], [transition_probabilities] and
    [root_frequencies]. With this variant, one can specify that some
    leaves are unobserved.
*)

val conditional_likelihoods :
  ('n, 'l, 'b) Tree.t ->
  nstates:int ->
  leaf_state:('l -> int) ->
  transition_probabilities:('b -> mat) ->
  (shifted_vector, int, 'b * mat) Tree.t
(** [conditional_likelihoods t ~nstates ~leaf_state ~transition_probabilities]
    computes the conditional likelihoods of observing the states returned
    by [leaf_state] at the leaves of [t] given the CTMC specified by
    [nstates] and [transition_probabilities].
*)

val conditional_simulation :
  Gsl.Rng.t ->
  (shifted_vector, int, 'b * mat) Tree.t ->
  root_frequencies:vec ->
  (int, int, 'b * mat) Tree.t
(** [conditional_simulation rng tree ~root_frequencies] performs a conditional
    simulation on the provided [tree] given the [root_frequencies] using the
    provided random number generator [rng].
*)

(** In this variant implementation, one can specify some uncertainty
    for a given leaf, by letting [leaf_state] return [true] for several
    states. In that case, it is understood that each state has equal
    probability. If [leaf_state] always returns [false] for a given
    leaf, it is interpreted as the leaf being unobserved. *)
module Ambiguous : sig
  val pruning :
    ('a, 'b, 'c) Tree.t ->
    nstates:int ->
    transition_probabilities:('c -> matrix_decomposition) ->
    leaf_state:('b -> int -> bool) ->
    root_frequencies:vec ->
    float
  (** [pruning t ~nstates ~transition_probabilities ~leaf_state
      ~root_frequencies] returns the probability of observing the
      states returned by [leaf_state] at the leaves of [t] given the
      CTMC specified by [nstates], [transition_probabilities] and
      [root_frequencies].
  *)

  val conditional_likelihoods :
    ('n, 'l, 'b) Tree.t ->
    nstates:int ->
    leaf_state:('l -> int -> bool) ->
    transition_probabilities:('b -> mat) ->
    (shifted_vector, int array, 'b * mat) Tree.t
  (** [conditional_likelihoods t ~nstates ~leaf_state ~transition_probabilities]
      computes the conditional likelihoods of observing the states returned
      by [leaf_state] at the leaves of [t] given the CTMC specified by
      [nstates] and [transition_probabilities].
  *)

  val conditional_simulation :
    Gsl.Rng.t ->
    (shifted_vector, int array, 'b * mat) Tree.t ->
    root_frequencies:vec ->
    (int, int, 'b * mat) Tree.t
    (** [conditional_simulation rng tree ~root_frequencies] performs a conditional
        simulation on the provided [tree] given the [root_frequencies] using the
        provided random number generator [rng].
    *)
end

module Uniformized_process : sig
  (**
     The Uniformized_process module provides functionality for creating and
     manipulating uniformized processes. A uniformized process is a continuous-time
     Markov chain (CTMC) that has been transformed into a time-homogeneous process
     with uniformized transition rates. This transformation simplifies certain
     calculations and simulations.

     In a CTMC, the transition rates between states can vary depending on the time
     duration of the transitions. This can make calculations and simulations more
     complex, especially when dealing with varying branch lengths or time intervals.

     The uniformization process transforms the CTMC into a time-homogeneous process
     with constant transition rates. By assuming a uniform time scale, where all
     transitions occur with a fixed rate, the uniformized process enables easier
     computations and simulations.

     This simplification allows for more efficient and straightforward calculations
     of probabilities, likelihoods, and other quantities associated with the CTMC.
     It facilitates the use of numerical methods and algorithms, such as matrix
     exponentiation, to analyze and simulate the behavior of the CTMC.

     The module defines a type 't' representing a uniformized process and provides
     functions for creating a uniformized process, accessing its transition rates,
     and transition probabilities.    
  *)

  type t
  val make :
    transition_rates:mat ->
    transition_probabilities:(float -> mat) ->
    (branch_length:float -> t) Core.Staged.t
  (** [make ~transition_rates ~transition_probabilities] constructs a uniformized
      process with the given [transition_rates] and [transition_probabilities].
      It returns a staged computation that takes the [branch_length] as an
      argument and produces the uniformized process [t].
  *)

  val transition_rates : t -> mat
  (** [transition_rates process] returns the transition rates matrix of the
      given [process].
  *)

  val transition_probabilities : t -> mat
  (** [transition_probabilities process] returns the transition probabilities
      matrix of the given [process].
  *)
end










module Path_sampler : sig

  (** The Path_sampler module provides functionality for sampling paths in continuous-time Markov chains (CTMCs). It offers various methods for sampling paths based on different algorithms.

       Sampling paths in a CTMC involves generating sequences of states and associated time durations, representing the evolution of the system over time. This process is useful for studying the behavior of CTMCs, analyzing probabilities, and performing simulations.

       The module offers the following path sampling methods:

      - Uniformization: The `uniformization` function transforms a CTMC into a uniformized process, simplifying path sampling by assuming a constant transition rate. This method is efficient for cases where uniformization is applicable.

      - Rejection Sampling: The `rejection_sampling` function performs path sampling using rejection sampling. It generates paths by repeatedly proposing transitions and accepting or rejecting them based on a rejection criterion. This method is suitable for general CTMCs but may require a large number of proposals.

      - Rejection Sampling or Uniformization: The `rejection_sampling_or_uniformization` function combines the benefits of rejection sampling and uniformization. It tries rejection sampling first and falls back to uniformization if the maximum number of rejection tries is reached.

       The module also provides a `sample_exn` function to sample a specific path between two given states in a CTMC using a chosen path sampling method.
  *)
  type t
  (** The type representing a path sampler. *)

  val uniformization : Uniformized_process.t -> t
  (** [uniformization uniformized_process] creates a path sampler using the uniformization method. It transforms a CTMC represented by the `uniformized_process` into a uniformized process, simplifying path sampling. The resulting `t` can be used for sampling paths using the `sample_exn` function. *)

  val rejection_sampling :
    max_tries:int ->
    rates:mat ->
    branch_length:float ->
    unit -> t
  (** [rejection_sampling ~max_tries ~rates ~branch_length ()] creates a path sampler using the rejection sampling method. It performs path sampling by proposing transitions based on the given transition rates (`rates`) and accepting or rejecting them based on a rejection criterion. The `max_tries` parameter specifies the maximum number of rejection tries allowed per path. The `branch_length` parameter represents the time duration for which the path is sampled. The resulting `t` can be used for sampling paths using the `sample_exn` function. *)

  val rejection_sampling_or_uniformization :
    max_tries:int ->
    Uniformized_process.t ->
    t
  (** [rejection_sampling_or_uniformization ~max_tries uniformized_process] creates a path sampler using the rejection sampling or uniformization method. It tries rejection sampling first and falls back to uniformization if the maximum number of rejection tries (`max_tries`) is reached. The `uniformized_process` represents the uniformized process to use in case of fallback. The resulting `t` can be used for sampling paths using the `sample_exn` function. *)

  val sample_exn :
    t ->
    rng:Gsl.Rng.t ->
    start_state:int ->
    end_state:int ->
    (int * float) array
    (** [sample_exn path_sampler ~rng ~start_state ~end_state] samples a path between the given `start_state` and `end_state` using the chosen path sampling method represented by `path_sampler`. The `rng` parameter is the random number generator used for sampling. The function returns an array representing the sampled path, where each element is a tuple `(state, duration)` indicating the state and the time duration spent in that state. This function may raise an exception if the sampling fails. *)
end

val substitution_mapping :
  rng:Gsl.Rng.t ->
  path_sampler:('b -> Path_sampler.t) ->
  (int, int, 'b * mat) Tree.t ->
  (int, int, 'b * (int * float) array) Tree.t
(** [substitution_mapping ~rng ~path_sampler tree] applies the substitution mapping algorithm to a given phylogenetic tree represented by [tree]. The substitution mapping process involves sampling paths along the tree branches based on the transition rates and probabilities associated with each branch.

    - The [rng] parameter is the random number generator used for sampling.
    - The [path_sampler] is a function that takes a state-specific parameter [b] and returns a path sampler of type [Path_sampler.t]. It determines the path sampling method to be used for each state-specific parameter.
    - The [tree] represents the phylogenetic tree structure, where each node contains a state-specific parameter [b] and a transition matrix [mat].

    The function returns a new phylogenetic tree structure where each transition matrix [mat] is replaced by an array of state-duration tuples [(int * float)], obtained by sampling a path along the corresponding branch using the specified path sampling method.
*)