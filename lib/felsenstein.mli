(** Functions that implement Felsenstein's "pruning" algorithm to compute
    likelihood of phylogenetic trees with known sequences at leaves. *)

(** Signature for alignment module.

    The `Alignment` module provides a signature for working with sequence
    alignments. It defines the types and operations required by the Felsenstein
    module to compute likelihoods for phylogenetic trees given alignments.

    The module implementing this signature should provide a type `t` representing
    an alignment, and a type `base` representing the individual bases or characters
    in the alignment. It should also implement the following functions:

    - [get_base] retrieves the base at a specific position in the alignment.
      It takes an alignment `t`, a string `seq` representing the identifier of the
      sequence, and an integer `pos` representing the position of the base.
      It returns the corresponding base.

    - [length] returns the length of the alignment.

    The `Alignment` module is used as a dependency in the `Make` functor of the
    Felsenstein module, allowing the Felsenstein algorithm to operate on different
    types of sequence alignments. By parametrizing the `Make` functor with a module
    that implements the `Alignment` interface, the Felsenstein module can work with
    various types of sequence alignments, such as DNA alignments, protein alignments,
    etc., as long as the alignment module provides the required functions.

    {[
      Example:

      module DNA_Alignment : Alignment with type base = DNA.base = struct
        type t = ...
        type base = DNA.base

        let get_base t ~seq ~pos = ...
          let length t = ...
      end
    ]} *)
module type Alignment = sig
  type t
  type base

  (** Get the base at a specific position in the alignment.

      [get_base t ~seq ~pos] retrieves the base at position [pos] in the
      alignment [t] corresponding to the sequence identified by [seq].

      - [t] is the alignment.

      - [seq] is the identifier of the sequence.

      - [pos] is the position of the base.

      Returns the base at the specified position.

      {[
        Example:

          let t = ...  (* Alignment *)
            let seq = ... (* Sequence identifier *)
              let pos = ... (* Position of the base *)
                let base = get_base t ~seq ~pos
      ]} *)
  val get_base : t -> seq:string -> pos:int -> base

  (** Get the length of the alignment.

      [length t] returns the length of the alignment [t].

      - [t] is the alignment.

      Returns the length of the alignment.

      {[
        Example:

          let t = ... (* Alignment *)
            let len = length t
      ]} *)
  val length : t -> int
end


module Make
    (A : Alphabet.S)
    (Align : Alignment with type base := A.t)
    (E : Site_evolution_model.S with type mat := A.matrix
                                 and type vec := A.vector) :
sig
  (** Single-site. Felsenstein without underflow prevention.

      [felsenstein_single ?shift param ~site tree seq] computes the likelihood
      of a phylogenetic tree [tree] given an alignment [seq] at a specific site
      [site] using the site evolution model [param]. This function does not
      perform underflow prevention, meaning it does not apply any adjustments
      to prevent numerical underflows when computing the likelihood.

      Underflow occurs when the probabilities become extremely small and approach
      zero, leading to loss of precision in floating-point arithmetic. By not
      performing underflow prevention, this function may produce inaccurate
      results or encounter numerical issues when computing likelihoods for
      sequences with very low probabilities.

      If you expect the likelihoods to be extremely small or encounter underflow
      issues, it is recommended to use [felsenstein_single_shift] or [felsenstein]
      instead, which perform underflow prevention by applying a shift to the
      probability vector.

      - [?shift] is an optional argument specifying a shift function that
        prevents underflow. The shift function takes three arguments:
        [shift shift_left shift_right vector], where [shift_left] and
        [shift_right] are the shifts accumulated from the left and right
        subtrees, and [vector] is the probability vector. The shift function
        returns a tuple [(new_vector, new_shift)], where [new_vector] is the
        shifted probability vector and [new_shift] is the updated shift.

      - [param] is the site evolution model parameter.

      - [site] is the index of the site in the alignment.

      - [tree] is the phylogenetic tree.

      - [seq] is the alignment.

      Returns the computed likelihood.

      {[
        Example:

          let param = ... (* Create the site evolution model parameter *)
            let tree = ...  (* Create the phylogenetic tree *)
              let seq = ...   (* Create the alignment *)
                let site = 0    (* Index of the site in the alignment *)
        let likelihood = felsenstein_single param ~site tree seq
      ]} *)
  val felsenstein_single :
    ?shift:(float -> float -> A.vector -> A.vector * float) ->
    E.param ->
    site:int ->
    Phylogenetic_tree.t ->
    Align.t ->
    float

  (** Single-site Felsenstein with underflow prevention.

      [felsenstein_single_shift ?threshold param ~site tree seq] computes the
      likelihood of a phylogenetic tree [tree] given an alignment [seq] at a
      specific site [site] using the site evolution model [param]. This function
      performs underflow prevention by applying a shift to the probability vector
      when the minimum value of the vector is below a certain threshold.

      - [?threshold] is an optional argument specifying the threshold value for
        underflow prevention. The default value is [0.0000001].

      - [param] is the site evolution model parameter.

      - [site] is the index of the site in the alignment.

      - [tree] is the phylogenetic tree.

      - [seq] is the alignment.

      Returns the computed likelihood.

      {[
        Example:

          let param = ... (* Create the site evolution model parameter *)
            let tree = ...  (* Create the phylogenetic tree *)
              let seq = ...   (* Create the alignment *)
                let site = 0    (* Index of the site in the alignment *)
        let likelihood = felsenstein_single_shift param ~site tree seq
      ]} *)
  val felsenstein_single_shift :
    ?threshold:float ->
    E.param ->
    site:int ->
    Phylogenetic_tree.t ->
    Align.t ->
    float

  (** Multisite Felsenstein without underflow prevention.

      [felsenstein_noshift param tree seq] computes the likelihood of a
      phylogenetic tree [tree] given an alignment [seq] using the site evolution
      model [param]. This function does not perform underflow prevention.

      - [param] is the site evolution model parameter.

      - [tree] is the phylogenetic tree.

      - [seq] is the alignment.

      Returns the computed likelihood.

      {[
        Example:

          let param = ... (* Create the site evolution model parameter *)
            let tree = ...  (* Create the phylogenetic tree *)
              let seq = ...   (* Create the alignment *)
                let likelihood = felsenstein_noshift param tree seq
      ]} *)
  val felsenstein_noshift :
    E.param ->
    Phylogenetic_tree.t ->
    Align.t ->
    float

  (** Multisite Felsenstein with underflow prevention (use this by default).

      [felsenstein param tree seq] computes the likelihood of a phylogenetic
      tree [tree] given an alignment [seq] using the site evolution model
      [param]. This function performs underflow prevention by applying a shift
      to the probability vector when the minimum value of the vector is below
      a certain threshold. This is the recommended function to use for computing
      likelihood across multiple sites.

      - [param] is the site evolution model parameter.

      - [tree] is the phylogenetic tree.

      - [seq] is the alignment.

      Returns the computed likelihood.

      {[
        Example:

          let param = ... (* Create the site evolution model parameter *)
            let tree = ...  (* Create the phylogenetic tree *)
              let seq = ...   (* Create the alignment *)
                let likelihood = felsenstein param tree seq
      ]} *)
  val felsenstein :
    E.param ->
    Phylogenetic_tree.t ->
    Align.t ->
    float
end
